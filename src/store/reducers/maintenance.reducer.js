import * as actionTypes from '../actionType';

const initialState = {
    currentMaintenanceRecords:  [],
    completedMaintenanceRecords: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_PAYMENT_LIST:
            return {
                ...state
            }

        case actionTypes.SET_MAINTENANCE_LIST:
            return {
                ...state,
                currentMaintenanceRecords: action.payload.currentMaintenanceRecords,
                completedMaintenanceRecords: action.payload.completedMaintenanceRecords
            }
        default:
            { }
    }
    return state;
};

export default reducer;