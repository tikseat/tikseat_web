import * as actionTypes from '../actionType';

const initialState = {
    secCounter: 0,
    api_data: {}
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD:
            return {
                ...state,
                secCounter: state.secCounter + action.val
            }
        case actionTypes.SUBTRACT:
            return {
                ...state,
                secCounter: state.secCounter - action.val
            }
        case actionTypes.GETDATA:
            return {
                ...state,
                api_data: action.payload
            }
        default:
            { }
    }
    return state;
};

export default reducer;