import * as actionTypes from '../actionType';

const initialState = {
    counter: 0,
    apiData: {},
    office_list: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.INCREMENT:
            return {
                ...state,
                counter: state.counter + 1
            }
        case actionTypes.DECREMENT:
            return {
                ...state,
                counter: state.counter - 1
            }
        case actionTypes.GET_OFFICE_LIST:
            return {
                ...state,
                office_list: action.payload,
            }

        default: {

        }
    }
    return state;
};

export default reducer;