import * as actionTypes from '../actionType';

const initialState = {
    applicants: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_APPLICANTS_LIST:
            return {
                ...state
            }

        case actionTypes.SET_APPLICANTS_LIST:
            return {
                ...state,
                applicants: action.payload
            }
        default:
            { }
    }
    return state;
};

export default reducer;