import * as actionTypes from '../actionType';

const initialState = {
    properties: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_PRTPERTY_LIST:
            return {
                ...state
            }

        case actionTypes.SET_PRTPERTY_LIST:
            return {
                ...state,
                properties: action.payload
            }
        default:
            { }
    }
    return state;
};

export default reducer;