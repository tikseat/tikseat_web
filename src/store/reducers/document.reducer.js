import * as actionTypes from '../actionType';

const initialState = {
    documents: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_DOCUMENT_LIST:
            return {
                ...state
            }
        case actionTypes.ADD_DOCUMENT:
            return {
                ...state,
                documents: [...state.documents,action.payload]
            }
        case actionTypes.SET_DOCUMENT_LIST:
            return {
                ...state,
                documents: action.payload
            }

        default:
            { }
    }
    return state;
};

export default reducer;