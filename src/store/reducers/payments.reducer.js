import * as actionTypes from '../actionType';

const initialState = {
    payments: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_PAYMENT_LIST:
            return {
                ...state
            }

        case actionTypes.SET_PAYMENT_LIST:
            return {
                ...state,
                payments: action.payload
            }
        default:
            { }
    }
    return state;
};

export default reducer;