import * as actionTypes from '../actionType';

const initialState = {
    tenants: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_TENANTS_SCREENING_LIST:
            return {
                ...state
            }

        case actionTypes.SET_TENANTS_SCREENING_LIST:
            return {
                ...state,
                tenants: action.payload
            }
        default:
            { }
    }
    return state;
};

export default reducer;