import { combineReducers } from 'redux';
import firstReducer from './reducers/first.reducer';
import secondReducer from './reducers/second.reducer';
import authReducer from './reducers/auth.reducer';
import propertiesReducer from './reducers/properties.reducer';
import paymentsReducer from './reducers/payments.reducer';
import applicantsReducer from './reducers/applicants.reducer';
import tenantsReducer from './reducers/tenants.reducer';
import maintenanceReducer from './reducers/maintenance.reducer';
import documentReducer from './reducers/document.reducer';
import * as actionTypes from './actionType';

// export const rootReducer = combineReducers({     // OLD rootreducer
//     auth: authReducer,
//     firstt: firstReducer,
//     secondd: secondReducer
// });

const appReducer = combineReducers({   // new rootreducer, clear store when user will logout
    auth: authReducer,
    firstt: firstReducer,
    secondd: secondReducer,
    properties: propertiesReducer,
    payments: paymentsReducer,
    applicants: applicantsReducer,
    tenants: tenantsReducer,
    maintenance: maintenanceReducer,
    documents: documentReducer,
});

export const rootReducer = (state, action) => {
    if (action.type === actionTypes.REMOVE_CREDENTIALS) {
        state = undefined;
    }
    return appReducer(state, action)
}