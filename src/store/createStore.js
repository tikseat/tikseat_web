import { applyMiddleware, compose, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';

import { rootReducer } from './rootReducer';


const loggerMiddleware = createLogger();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

let store;
if (process.env.REACT_APP_ENV === 'local') {
    store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk, loggerMiddleware)));
} else {
    store = createStore(rootReducer, applyMiddleware(thunk));
    if (window) {
        window.console.log = function () { };
    }
}

export default store