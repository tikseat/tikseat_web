import * as actionTypes from '../actionType';

export const getApplicantsList = () => {
    return {
        type: actionTypes.GET_APPLICANTS_LIST
    };
};

export const setApplicantsList = (payload) => {
    return {
        type: actionTypes.SET_APPLICANTS_LIST,
        payload
    };
};
