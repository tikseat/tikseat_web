import * as actionTypes from '../actionType';

export const getTenantsList = () => {
    return {
        type: actionTypes.GET_TENANTS_SCREENING_LIST
    };
};

export const setTenantList = (payload) => {
    return {
        type: actionTypes.SET_TENANTS_SCREENING_LIST,
        payload
    };
};
