import * as actionTypes from '../actionType';

export const getDocumentList = () => {
    return {
        type: actionTypes.GET_DOCUMENT_LIST
    };
};

export const addDocument = (doc) => {
    return {
        type: actionTypes.ADD_DOCUMENT,
        payload: doc
    };
};

export const setDocumentList = (payload) => {
    return {
        type: actionTypes.SET_DOCUMENT_LIST,
        payload
    };
};
