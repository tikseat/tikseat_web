import * as actionTypes from '../actionType';

export const getPaymentList = () => {
    return {
        type: actionTypes.GET_PAYMENT_LIST
    };
};

export const setPaymentList = (payload) => {
    return {
        type: actionTypes.SET_PAYMENT_LIST,
        payload
    };
};
