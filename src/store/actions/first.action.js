import * as actionTypes from '../actionType';
import api from '../../api';

export const increment = () => {
    return {
        type: actionTypes.INCREMENT
    };
};

export const decrement = () => {
    return {
        type: actionTypes.DECREMENT
    };
};


export const getUsersData = (value) => {
    return (dispatch, getState) => {
        // dispatch({
        //     type: actionTypes.LOGIN_ATTEMPT
        // })
        api.get('/office').then(res => {
            dispatch({
                type: actionTypes.GET_OFFICE_LIST,
                payload: res.data
            });
        }).catch(err => {
            // dispatch({
            //     type: actionTypes.LOGIN_FAILED
            // })
        })
    }
};

