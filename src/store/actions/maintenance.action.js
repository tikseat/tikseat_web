import * as actionTypes from '../actionType';

export const getCurrentMaintenaceList = () => {
    return {
        type: actionTypes.GET_CURRENT_MAINTENANCE_LIST
    };
};

export const getCompletedMaintenaceList = () => {
    return {
        type: actionTypes.GET_COMPLETED_MAINTENANCE_LIST
    };
};

export const setMaintananceList = (payload) => {
    return {
        type: actionTypes.SET_MAINTENANCE_LIST,
        payload
    };
};
