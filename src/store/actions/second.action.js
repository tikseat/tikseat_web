import server from '../../api';
import * as actionTypes from '../actionType';
export const add = (value) => {
    return {
        type: actionTypes.ADD,
        val: value
    };
};

export const subtract = (value) => {
    return {
        type: actionTypes.SUBTRACT,
        val: value
    };
};

export const getData = (res) => {
    return (dispatch, getState) => {
        // console.log(getState());
        server.get('/5185415ba171ea3a00704eed').then(res => {
            return dispatch({
                type: actionTypes.GETDATA,
                payload: res.data
            })
        }).catch(err => {
            console.log('err', err)
        })
    }
};