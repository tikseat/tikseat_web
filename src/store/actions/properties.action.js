import * as actionTypes from '../actionType';

export const getProperty = () => {
    return {
        type: actionTypes.GET_PRTPERTY_LIST
    };
};

export const setPropertyList = (payload) => {
    return {
        type: actionTypes.SET_PRTPERTY_LIST,
        payload
    };
};
