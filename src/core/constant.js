export const API_ENDPOINT = [];

export const USER_TYPES = {
    admin: 'admin',
    client: 'client'
};

export const CONFIRMATION_MSG = {
    request: {
        accept: {
            message: 'Are you sure want to accept this request?',
            okBtnTxt: 'Confirm',
            cancelBtnTxt: 'Close'
        },
        reject: {
            message: 'Are you sure want to decline this request?',
            okBtnTxt: 'Confirm',
            cancelBtnTxt: 'Close'
        }
    },       
    category: {
        delete: {
            message: 'Are you sure want to delete this category?',
            okBtnTxt: 'Yes',
            cancelBtnTxt: 'No'
        },
        active: {
            message: 'Are you sure want to active this category?',
            okBtnTxt: 'Yes',
            cancelBtnTxt: 'No'
        },
        deactive: {
            message: 'Are you sure want to deactive this category?',
            okBtnTxt: 'Yes',
            cancelBtnTxt: 'No'
        }
    },       
    store: {
        delete: {
            message: 'Are you sure want to delete this store?',
            okBtnTxt: 'Yes',
            cancelBtnTxt: 'No'
        },
        active: {
            message: 'Are you sure want to active this store?',
            okBtnTxt: 'Yes',
            cancelBtnTxt: 'No'
        },
        deactive: {
            message: 'Are you sure want to deactive this store?',
            okBtnTxt: 'Yes',
            cancelBtnTxt: 'No'
        }
    },       
    user: {
        delete: {
            message: 'Are you sure want to delete this user?',
            okBtnTxt: 'Yes',
            cancelBtnTxt: 'No'
        },
        active: {
            message: 'Are you sure want to active this user?',
            okBtnTxt: 'Yes',
            cancelBtnTxt: 'No'
        },
        deactive: {
            message: 'Are you sure want to deactive this user?',
            okBtnTxt: 'Yes',
            cancelBtnTxt: 'No'
        }
    }
}

export const LOCAL_STORAGE_VAR = {
    credentialsKey: 'tikseat_console_credentials',
};

