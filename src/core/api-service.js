import api from './../api';
import { getParams } from './util';
import store from './../store/createStore';
import * as actionCreators from './../store/actions';

export default (config) => {
    return new Promise((resolve, reject) => {
        const method = config.method;
        let apiData;
        if (method === 'put' || method === 'post') {
            apiData = api[method](config.queryParam ? config.url + getParams(config.queryParam) : config.url, config.payload);
        } else {
            apiData = api[method](config.url);
        }
        const storeData = store.getState();
        if (storeData && storeData.auth.serverErrors) {
            store.dispatch(actionCreators.clearErrors());
        }
        apiData.then(res => {
            if (res.data) {
                resolve(res.data)
            } else {
                reject(res);
            }
        }).catch(err => {
            if (err && err.response && err.response.data) {
                reject(err.response.data);
                if (store) {
                    store.dispatch(actionCreators.addErrors(err.response.data));
                }
            } else {
                reject(err);
            }
        })
    })
}