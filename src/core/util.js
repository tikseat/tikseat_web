// inArray(key, Array)
export const inArray = (needle, haystack) => {
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (haystack[i] === needle) return true;
    }
    return false;
}


export const getParams = (filterObject) => {
    const filterKeys = Object.keys(filterObject);
    let queryString;
    if (filterKeys.length > 0) {
        queryString = '?';
    }
    filterKeys.forEach((key, index) => {
        if (filterObject[key] !== undefined && filterObject[key] !== null) {
            if (typeof filterObject[key] === 'object') {
                filterObject[key].forEach((value) => {
                    queryString += `${key}=${value}&`;
                });
            } else {
                if (filterObject[key].toString().length) {
                    queryString += `${key}=${filterObject[key]}&`;
                }
            }
        }
    });
    if (
        filterKeys.length > 0 &&
        queryString[queryString.length - 1] === '&'
    ) {
        queryString = queryString.slice(0, -1);
    }
    return queryString
}