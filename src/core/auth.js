import { useEffect, useState } from 'react';
import store  from './../store/createStore';
import * as actionCreators from './../store/actions';
import * as constant from './constant';

const Auth = () => {
    const [isViewLoaded, setIsViewLoaded] = useState(false);
    if (!isViewLoaded) {
        const savedCredentials = localStorage.getItem(constant.LOCAL_STORAGE_VAR.credentialsKey);
        if (savedCredentials) {
            store.dispatch(actionCreators.refresh(JSON.parse(savedCredentials)));
            refresh();
        }
    }
    useEffect(() => {
        setIsViewLoaded(true);
    }, []);
    return (null);
}

export default Auth;

export const isAuthenticated = () => {
    if (localStorage.getItem(constant.LOCAL_STORAGE_VAR.credentialsKey)) {
        return true;       
    } else {
        return false;
    }
}

export const getAuthenticatedUser = () => {
    const loggedInUser = localStorage.getItem(constant.LOCAL_STORAGE_VAR.credentialsKey);
    if (loggedInUser) {
        return JSON.parse(loggedInUser);
    } else {
        return null;
    }
}

export const setCredentials = (credentials) => {
    if (credentials) {
        localStorage.setItem(constant.LOCAL_STORAGE_VAR.credentialsKey, JSON.stringify(credentials));
    } else {
        localStorage.removeItem(constant.LOCAL_STORAGE_VAR.credentialsKey);
    }
}

export const refresh = () => {
    let userCredentials = getAuthenticatedUser();
    // mutate(POST_ME_MUTATION, {}).then((response) => {
    //     if (response.auth && response.auth.me) {
    //         userCredentials.user = response.auth.me;
    //         setCredentials(userCredentials);
    //         store.dispatch(actionCreators.refresh(userCredentials));
    //     }
    // })
}