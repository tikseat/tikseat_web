import React from 'react';
// import { NavLink } from 'react-router-dom';
// import { connect } from 'react-redux';
import '../../assets/scss/admin/dashboard.scss';
import Modal from 'react-bootstrap/Modal';
// import * as constant from '../../core/constant';
// import Confirmation from '../shared/modals/Confirmation';

const ViewRequest = (props) => {
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            View Request
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h5>Request For : New Service Added</h5>
          <p>Haircut (100), Facial (450)</p>
        </Modal.Body>
        {/* <Modal.Footer className="justify-content-center">
          <button className="btn btn-success " onClick={props.onHide}><i className="fas fa-check"></i> Accept</button>
          <button className="btn btn-danger" onClick={props.onHide}><i className="fas fa-times"></i> Decline</button>
        </Modal.Footer> */}
      </Modal>
    );
  }

const BusinessDashboard = (props) => {
    const [modalViewRequestShow, setModalViewRequestShow] = React.useState(false);
    // const [modalConfirmationShow, setModalConfirmationShow] = React.useState(false);

    return (
        <div className="container-fluid">
            <div className="content-max ">
                <div className="d-flex justify-content-between dashboard-widgets">
                    <div className="dashboard-widget dashboard-widget-1">
                        <div className="d-flex align-items-center">
                            <i className="fab fa-gratipay"></i>
                            <div className="widget-count">
                                <h2>10000</h2>
                                <span>Likes</span>
                            </div>
                        </div>
                    </div>
                    <div className="dashboard-widget dashboard-widget-2">
                        <div className="d-flex align-items-center">
                            <i className="fas fa-star-half-alt"></i>
                            <div className="widget-count">
                                <h2>500</h2>
                                <span>Rating/Reviews</span>
                            </div>
                        </div>
                    </div>
                    <div className="dashboard-widget dashboard-widget-3">
                        <div className="d-flex align-items-center">
                            <i className="fas fa-users"></i>
                            <div className="widget-count">
                                <h2>600</h2>
                                <span>Total Users</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="card">
                    <div className="card-header">
                        <h3><i className="fas fa-exchange-alt"></i> Open Requests</h3>
                    </div>
                    <div className="card-body p-0">
                        <div className="table">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Request for</th>
                                        <th>Date Time</th>
                                        <th>Status</th>
                                        <th className="text-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>New Services Added</td>
                                        <td>01-08-2019 - 02:00 AM</td>
                                        <td>Pending</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="View Request" onClick={() => setModalViewRequestShow(true)}><i className="far fa-eye"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>New Services Added</td>
                                        <td>01-08-2019 - 02:00 AM</td>
                                        <td>Pending</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="View Request" onClick={() => setModalViewRequestShow(true)}><i className="far fa-eye"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>New Services Added</td>
                                        <td>01-08-2019 - 02:00 AM</td>
                                        <td>Pending</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="View Request" onClick={() => setModalViewRequestShow(true)}><i className="far fa-eye"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>New Services Added</td>
                                        <td>01-08-2019 - 02:00 AM</td>
                                        <td>Pending</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="View Request" onClick={() => setModalViewRequestShow(true)}><i className="far fa-eye"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>New Services Added</td>
                                        <td>01-08-2019 - 02:00 AM</td>
                                        <td>Pending</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="View Request" onClick={() => setModalViewRequestShow(true)}><i className="far fa-eye"></i></button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <ViewRequest show={modalViewRequestShow} onHide={() => setModalViewRequestShow(false)} />
        </div>  
    )
}

export default BusinessDashboard;
