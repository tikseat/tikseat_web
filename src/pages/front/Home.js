import React from 'react';
import StoreSingle from '../../shared/front/StoreSingle';
import ProfessionalSingle from '../../shared/front/ProfessionalSingle';

const Home = (props) => {
    let storeConfig = {
        isPopularStoreView: true,
    }
    let professionalConfig = {
        isPopularProfessionalView: true,
    }
    return (
        
        <div className="page-container d-flex justify-content-between">
            <div className="flex-1 popular-business-container mr-3">
                <h4>Popular Businesses</h4>
                <div className="store-listing-container">
                    <StoreSingle config={storeConfig} />
                    <StoreSingle config={storeConfig} />
                    <StoreSingle config={storeConfig} />
                    <StoreSingle config={storeConfig} />
                </div>
            </div>
            <div className="flex-1 popular-professional-container">
                <h4>Popular Professionals</h4>
                <div className="store-listing-container">
                    <ProfessionalSingle config={professionalConfig} />
                    <ProfessionalSingle config={professionalConfig} />
                    <ProfessionalSingle config={professionalConfig} />
                    <ProfessionalSingle config={professionalConfig} />
                </div>
            </div>  
        </div>
    )
}

export default Home;
