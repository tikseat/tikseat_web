import React from 'react';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import slider1 from "../../assets/img/store/slider/slider-1.jpg"
import slider2 from "../../assets/img/store/slider/slider-2.jpg"
import slider3 from "../../assets/img/store/slider/slider-3.jpg"
import slider4 from "../../assets/img/store/slider/slider-4.jpg"
import portfolio1 from "../../assets/img/professional/image1.jpg"
import portfolio2 from "../../assets/img/professional/image2.jpg"
import portfolio3 from "../../assets/img/professional/image3.jpg"
import portfolio4 from "../../assets/img/professional/image4.jpg"
import portfolio5 from "../../assets/img/professional/image5.jpg"
import '../../assets/scss/front/storeDetails.scss';
import Rating from '../../shared/front/Rating';
import {OverlayTrigger, Tooltip, Tabs, Tab, Row, Col, Nav} from 'react-bootstrap';

const StoreDetails = (props) => {
  const [isFavorite, setIsFavorite] = React.useState(false);
    let settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
      
        <div className="container-fluid store-details-container">
          <div className="slider-contianer">
            <Slider {...settings}>
              <div>
                <img src={slider1} alt="slider-1" />
              </div>
              <div>
                <img src={slider2} alt="slider-2" />
              </div>
              <div>
                <img src={slider3} alt="slider-3" />
              </div>
              <div>
                <img src={slider4} alt="slider-4" />
              </div>
            </Slider> 
            <div className="store-info-container">
              <div className="d-flex justify-content-between">
                <div className="store-title-bar">
                  <h3>Shree Chamunda Hair Style</h3>
                  <Rating />
                  <h6 className="mt-4"><i className="fas fa-store-alt mr-2"></i> Arbuda nagar, Odhav, Ahmedabad - 382415</h6>
                  <h5 className="mt-2"><i className="fas fa-phone rotate-phone-icon mr-2"></i> +91 9727190205, +91 8733854716</h5>
                </div>
                <div className="store-actions">
                  <OverlayTrigger placement="left" overlay={<Tooltip id="tooltip-favorite">{(isFavorite) ? "Favorited" : 'Favorite'}</Tooltip>}>
                    <i className={(isFavorite) ? "fas fa-heart mb-4 d-block cursor-pointer active" : "far d-block cursor-pointer mb-4 fa-heart"} onClick={() => setIsFavorite((isFavorite) ? false : true)}></i>
                  </OverlayTrigger>
                  <OverlayTrigger placement="left" overlay={<Tooltip  id="tooltip-share">Share</Tooltip>}>
                    <i className="fas fa-share-alt d-block mb-4 cursor-pointer"></i>
                  </OverlayTrigger>
                  <OverlayTrigger placement="left" overlay={<Tooltip  id="tooltip-map">Map</Tooltip>}>
                    <i className="fas fa-map-marker-alt mb-4 d-block cursor-pointer"></i>
                  </OverlayTrigger>
                  <OverlayTrigger placement="left" overlay={<Tooltip  id="tooltip-whatsapp">WhatsApp</Tooltip>}>
                    <i className="fab fa-whatsapp d-block mb-4 cursor-pointer"></i>
                  </OverlayTrigger>
                </div>
              </div>
            </div>
          </div>
          <div className="store-details-content">
          <Tab.Container id="store-details-tabs" defaultActiveKey="details">
            <Row>
              <Col sm={3}>
                <Nav variant="pills" className="flex-column">
                  <Nav.Item>
                    <Nav.Link eventKey="details"><i className="fas fa-info-circle"></i> Details</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="services"><i className="fas fa-tasks"></i> Services</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="offers"><i className="fas fa-gift"></i> Offers</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="packages"><i className="fas fa-gifts"></i> Packages</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="review"><i className="fas fa-star-half-alt"></i> Ratings & Reviews</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="portfolio"><i className="fas fa-briefcase"></i> Portfolio</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="gallery"><i className="fas fa-images"></i> Gallery</Nav.Link>
                  </Nav.Item>
                </Nav>
              </Col>
              <Col sm={9} className="pl-0">
                <Tab.Content>
                  <Tab.Pane eventKey="details">
                    
                  </Tab.Pane>
                  <Tab.Pane eventKey="services">
                    Services
                  </Tab.Pane>
                  <Tab.Pane eventKey="offers">
                    Offers
                  </Tab.Pane>
                  <Tab.Pane eventKey="packages">
                    Packages
                  </Tab.Pane>
                  <Tab.Pane eventKey="review">
                    Ratings & Reviews
                  </Tab.Pane>
                  <Tab.Pane eventKey="portfolio">
                    <div className="portfolio-grid d-flex align-items-start justify-content-between flex-wrap">
                      <div className="portfolio-single">
                        <img src={portfolio1} alt="portfolio-1" />
                        <div className="caption">
                          <h3>Lorem Ipsum</h3>
                        </div>
                      </div>
                      <div className="portfolio-single">
                        <img src={portfolio2} alt="portfolio-2" />
                        <div className="caption">
                          <h3>Lorem Ipsum</h3>
                        </div>
                      </div>
                      <div className="portfolio-single">
                        <img src={portfolio3} alt="portfolio-3" />
                        <div className="caption">
                          <h3>Lorem Ipsum</h3>
                        </div>
                      </div>
                      <div className="portfolio-single">
                        <img src={portfolio4} alt="portfolio-4" />
                        <div className="caption">
                          <h3>Lorem Ipsum</h3>
                        </div>
                      </div>
                      <div className="portfolio-single">
                        <img src={portfolio5} alt="portfolio-5" />
                        <div className="caption">
                          <h3>Lorem Ipsum</h3>
                        </div>
                      </div>
                      <div className="portfolio-single">
                        <img src={portfolio1} alt="portfolio-1" />
                        <div className="caption">
                          <h3>Lorem Ipsum</h3>
                        </div>
                      </div>
                      <div className="portfolio-single">
                        <img src={portfolio2} alt="portfolio-2" />
                        <div className="caption">
                          <h3>Lorem Ipsum</h3>
                        </div>
                      </div>
                      <div className="portfolio-single">
                        <img src={portfolio3} alt="portfolio-3" />
                        <div className="caption">
                          <h3>Lorem Ipsum</h3>
                        </div>
                      </div>
                      <div className="portfolio-single">
                        <img src={portfolio4} alt="portfolio-4" />
                        <div className="caption">
                          <h3>Lorem Ipsum</h3>
                        </div>
                      </div>
                      <div className="portfolio-single">
                        <img src={portfolio5} alt="portfolio-5" />
                        <div className="caption">
                          <h3>Lorem Ipsum</h3>
                        </div>
                      </div>
                    </div>
                  </Tab.Pane>
                  <Tab.Pane eventKey="gallery">
                    <div className="gallery-grid d-flex align-items-start justify-content-between flex-wrap">
                        <div className="gallery-single">
                          <img src={slider1} alt="gallery-1" />
                        </div>
                        <div className="gallery-single">
                          <img src={slider2} alt="gallery-2" />
                        </div>
                        <div className="gallery-single">
                          <img src={slider3} alt="gallery-3" />
                        </div>
                        <div className="gallery-single">
                          <img src={slider4} alt="gallery-4" />
                        </div>
                        <div className="gallery-single">
                          <img src={slider1} alt="gallery-1" />
                        </div>
                        <div className="gallery-single">
                          <img src={slider2} alt="gallery-2" />
                        </div>
                        <div className="gallery-single">
                          <img src={slider3} alt="gallery-3" />
                        </div>
                        <div className="gallery-single">
                          <img src={slider4} alt="gallery-4" />
                        </div>
                        <div className="gallery-single">
                          <img src={slider1} alt="gallery-1" />
                        </div>
                        <div className="gallery-single">
                          <img src={slider2} alt="gallery-2" />
                        </div>
                        <div className="gallery-single">
                          <img src={slider3} alt="gallery-3" />
                        </div>
                        <div className="gallery-single">
                          <img src={slider4} alt="gallery-4" />
                        </div>
                    </div>
                  </Tab.Pane>
                </Tab.Content>
              </Col>
            </Row>
          </Tab.Container>
          </div>
        </div>
    )
}

export default StoreDetails;
