import React from 'react';
import AdvertBanner from '../../shared/front/AdvertBanner';
import SortBar from '../../shared/front/SortBar';
import '../../assets/scss/front/listing.scss';
import Categories from '../../shared/front/Categories';
import StoreSingle from '../../shared/front/StoreSingle';
import ProfessionalSingle from '../../shared/front/ProfessionalSingle';

const Listing = (props) => {
    const [isMapView, setIsMapView] = React.useState(false);
    const [isProfiessionalView, setIsProfiessionalView] = React.useState(false);
    const [isFilterOpen, setIsFilterOpen] = React.useState(false);
    let storeConfig = {
        isPopularStoreView: false,
    }
    let professionalConfig = {
        isPopularProfessionalView: false,
    }
    return (
        <div className="page-container">
            <AdvertBanner />
            <div className="listing-page-container">
                <h4 className="page-title d-flex align-items-center justify-content-between">
                    <span><b>Saloon</b> in Arbudanagar Odhav</span>
                    <div className="d-flex align-items-center justify-content-end listing-icons">
                        <i className={ (isFilterOpen) ? 'active fas fa-filter mr-2 cursor-pointer' : 'fas fa-filter mr-2 cursor-pointer' } onClick={() => setIsFilterOpen((isFilterOpen) ? false : true)}></i>
                        <i className={ (isProfiessionalView) ? 'active fas fa-user-tie mr-2 cursor-pointer' : 'fas fa-user-tie mr-2 cursor-pointer' } onClick={() => setIsProfiessionalView((isProfiessionalView) ? false : true)}></i>
                        { (isMapView)  &&
                            <i className="fas fa-list-alt cursor-pointer" onClick={() => setIsMapView(false)}></i>
                        }
                        { (!isMapView)  &&
                            <i className="fas fa-map-marker-alt cursor-pointer" onClick={() => setIsMapView(true)}></i>
                        } 
                    </div>
                </h4>
                { (!isMapView && isFilterOpen) && 
                    <SortBar />
                }
                <div className="listing-page-content d-flex justify-content-between">
                    { (!isMapView) && 
                    <div className="category-list-container">
                        <Categories />
                    </div> }
                    { (isMapView) && 
                    <div className="map-container">
                        Map View
                    </div>
                    }
                    { (!isMapView && !isProfiessionalView) && 
                    <div className="store-list-container flex-1">
                        <StoreSingle config={storeConfig} />
                        <StoreSingle config={storeConfig} />
                        <StoreSingle config={storeConfig} />
                        <StoreSingle config={storeConfig} />
                        <StoreSingle config={storeConfig} />
                        <StoreSingle config={storeConfig} />
                        <StoreSingle config={storeConfig} />
                        <StoreSingle config={storeConfig} />
                        <StoreSingle config={storeConfig} />
                        <StoreSingle config={storeConfig} />
                        <StoreSingle config={storeConfig} />
                        <StoreSingle config={storeConfig} />
                    </div>
                    }
                    { (!isMapView && isProfiessionalView) && 
                    <div className="professional-list-container flex-1">
                        <ProfessionalSingle config={professionalConfig} />
                        <ProfessionalSingle config={professionalConfig} />
                        <ProfessionalSingle config={professionalConfig} />
                        <ProfessionalSingle config={professionalConfig} />
                        <ProfessionalSingle config={professionalConfig} />
                        <ProfessionalSingle config={professionalConfig} />
                        <ProfessionalSingle config={professionalConfig} />
                        <ProfessionalSingle config={professionalConfig} />
                        <ProfessionalSingle config={professionalConfig} />
                        <ProfessionalSingle config={professionalConfig} />
                        <ProfessionalSingle config={professionalConfig} />
                        <ProfessionalSingle config={professionalConfig} />
                    </div>
                    }
                </div>
            </div>
        </div>
    )
}

export default Listing;
