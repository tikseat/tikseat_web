import React from 'react';
import { connect } from 'react-redux';
import Modal from 'react-bootstrap/Modal';
import * as constant from '../../core/constant';
import Confirmation from '../shared/modals/Confirmation';

const AddEditUserModal = (props) => {
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Add User
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="form-container">
              <div className="form-group">
                  <label className="col-3">Name</label>
                  <div className="col-9">
                    <input type="text" name="name" className="form-control" />
                  </div>
              </div>
              <div className="form-group">
                  <label className="col-3">Mobile</label>
                  <div className="col-9">
                    <input type="text" name="mobile" className="form-control" />
                  </div>
              </div>
              <div className="form-group">
                  <label className="col-3">Email (Optional)</label>
                  <div className="col-9">
                    <input type="email" name="email" className="form-control" />
                  </div>
              </div>
          </div>
        </Modal.Body>
        <Modal.Footer className="justify-content-end">
          <button className="btn btn-success " onClick={props.onHide}><i className="fas fa-check"></i> Add</button>
        </Modal.Footer>
      </Modal>
    );
  }

const Users = (props) => {
    const [modalConfirmationShow, setModalConfirmationShow] = React.useState(false);
    const [modalUserShow, setModalUserShow] = React.useState(false);
    const [confirmationObj, setConfirmationObj] = React.useState({});

    const setConfirmationData = (type) => {
        setModalConfirmationShow(true);
        setConfirmationObj(constant.CONFIRMATION_MSG.user[type]);
    };

    return (
        <div className="container-fluid">
            <div className="content-max">
                <div className="card">
                    <div className="card-header">
                        <div className="d-flex align-items-center justify-content-between">
                            <h3><i className="fas fa-users"></i> Users</h3>
                            <div className="actions">
                                <button className="btn btn-sm btn-secondary" onClick={() => setModalUserShow(true)}><i className="fas fa-plus"></i> Add User</button>
                            </div>
                        </div>
                    </div>
                    <div className="card-body p-0">
                        <div className="table">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Mobile Number</th>
                                        <th>Email (Optional)</th>
                                        <th className="text-right">Created At</th>
                                        <th className="text-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Mayur Patel </td>
                                        <td>9584756892</td>
                                        <td>patelmayur7008@gmail.com</td>
                                        <td className="text-right">02-08-2019 04:00 PM</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-danger btn-outline mr-1" title="Ban User" onClick={() => setConfirmationData('deactive')}><i className="fas fa-ban"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit User" onClick={() => setModalUserShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete User" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Prince Patel </td>
                                        <td>9584756892</td>
                                        <td>patelmayur7008@gmail.com</td>
                                        <td className="text-right">02-08-2019 04:00 PM</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-success btn-outline mr-1" title="Active User" onClick={() => setConfirmationData('active')}><i className="fas fa-check"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit User" onClick={() => setModalUserShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete User" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mayur Patel </td>
                                        <td>9584756892</td>
                                        <td>patelmayur7008@gmail.com</td>
                                        <td className="text-right">02-08-2019 04:00 PM</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-danger btn-outline mr-1" title="Ban User" onClick={() => setConfirmationData('deactive')}><i className="fas fa-ban"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit User" onClick={() => setModalUserShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete User" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Prince Patel </td>
                                        <td>9584756892</td>
                                        <td>patelmayur7008@gmail.com</td>
                                        <td className="text-right">02-08-2019 04:00 PM</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-success btn-outline mr-1" title="Active User" onClick={() => setConfirmationData('active')}><i className="fas fa-check"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit User" onClick={() => setModalUserShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete User" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mayur Patel </td>
                                        <td>9584756892</td>
                                        <td>patelmayur7008@gmail.com</td>
                                        <td className="text-right">02-08-2019 04:00 PM</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-danger btn-outline mr-1" title="Ban User" onClick={() => setConfirmationData('deactive')}><i className="fas fa-ban"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit User" onClick={() => setModalUserShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete User" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Prince Patel </td>
                                        <td>9584756892</td>
                                        <td>patelmayur7008@gmail.com</td>
                                        <td className="text-right">02-08-2019 04:00 PM</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-success btn-outline mr-1" title="Active User" onClick={() => setConfirmationData('active')}><i className="fas fa-check"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit User" onClick={() => setModalUserShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete User" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mayur Patel </td>
                                        <td>9584756892</td>
                                        <td>patelmayur7008@gmail.com</td>
                                        <td className="text-right">02-08-2019 04:00 PM</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-danger btn-outline mr-1" title="Ban User" onClick={() => setConfirmationData('deactive')}><i className="fas fa-ban"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit User" onClick={() => setModalUserShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete User" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Prince Patel </td>
                                        <td>9584756892</td>
                                        <td>patelmayur7008@gmail.com</td>
                                        <td className="text-right">02-08-2019 04:00 PM</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-success btn-outline mr-1" title="Active User" onClick={() => setConfirmationData('active')}><i className="fas fa-check"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit User" onClick={() => setModalUserShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete User" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mayur Patel </td>
                                        <td>9584756892</td>
                                        <td>patelmayur7008@gmail.com</td>
                                        <td className="text-right">02-08-2019 04:00 PM</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-danger btn-outline mr-1" title="Ban User" onClick={() => setConfirmationData('deactive')}><i className="fas fa-ban"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit User" onClick={() => setModalUserShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete User" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Prince Patel </td>
                                        <td>9584756892</td>
                                        <td>patelmayur7008@gmail.com</td>
                                        <td className="text-right">02-08-2019 04:00 PM</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-success btn-outline mr-1" title="Active User" onClick={() => setConfirmationData('active')}><i className="fas fa-check"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit User" onClick={() => setModalUserShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete User" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="card-footer">
                        <div className="d-flex align-items-center justify-content-between">
                            <span>1 - 10 from 36 results</span>
                            <div className="d-flex">
                                <button className="btn btn-sm btn-grey btn-outline mr-1">Prev</button>
                                <button className="btn btn-sm btn-grey btn-outline">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
                <Confirmation show={modalConfirmationShow} data={confirmationObj} onHide={() => setModalConfirmationShow(false)} />
                <AddEditUserModal show={modalUserShow} onHide={() => setModalUserShow(false)} />
            </div>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        loggedInUser: '',
    }
};
const mapDispatchToProps = dispatch => {
    return {
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Users);

