import React, { useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import '../../assets/scss/admin/store-menus.scss';

const StoreDetail = (props) => {

    return (
        <div className="container-fluid">
            <div className="content-max">
                <div className="store-menus-container">
                    <NavLink className="menu-single" to={`/admin/stores/testId/basicInfo`}>
                        <div className="menu-icon">
                            <i className="fas fa-info-circle"></i>
                        </div>
                        <h3>Basic Information</h3>
                        <p>Description add here</p>
                    </NavLink>
                    <NavLink className="menu-single" to={`/admin/stores/testId/locationInfo`}>
                        <div className="menu-icon">
                            <i className="fas fa-map-marker-alt"></i>
                        </div>
                        <h3>Location Information</h3>
                        <p>Description add here</p>
                    </NavLink>
                    <NavLink className="menu-single" to={`/admin/stores/testId/contactInfo`}>
                        <div className="menu-icon">
                            <i className="fas fa-id-card-alt"></i>
                        </div>
                        <h3>Contact Information</h3>
                        <p>Description add here</p>
                    </NavLink>
                    <NavLink className="menu-single" to={`/admin/stores/testId/services`}>
                        <div className="menu-icon">
                            <i className="fas fa-tasks"></i>
                        </div>
                        <h3>Services</h3>
                        <p>Description add here</p>
                    </NavLink>
                    <NavLink className="menu-single" to={`/admin/stores/testId/offers`}>
                        <div className="menu-icon">
                            <i className="fas fa-gift"></i>
                        </div>
                        <h3>Offers</h3>
                        <p>Description add here</p>
                    </NavLink>
                    <NavLink className="menu-single" to={`/admin/stores/testId/packages`}>
                        <div className="menu-icon">
                            <i className="fas fa-gifts"></i>
                        </div>
                        <h3>Packages</h3>
                        <p>Description add here</p>
                    </NavLink>
                    <NavLink className="menu-single" to={`/admin/stores/testId/gallery`}>
                        <div className="menu-icon">
                            <i className="fas fa-images"></i>
                        </div>
                        <h3>Gallery</h3>
                        <p>Description add here</p>
                    </NavLink>
                    <NavLink className="menu-single" to={`/admin/stores/testId/portfolio`}>
                        <div className="menu-icon">
                            <i className="fas fa-briefcase"></i>
                        </div>
                        <h3>Portfolio</h3>
                        <p>Description add here</p>
                    </NavLink>
                </div>
            </div>
        </div>
    )
}

export default StoreDetail;
