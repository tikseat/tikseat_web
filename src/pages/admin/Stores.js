import React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import Modal from 'react-bootstrap/Modal';
import * as constant from '../../core/constant';
import Confirmation from '../shared/modals/Confirmation';

const AddEditStoreModal = (props) => {
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Add Store
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="form-container">
              <div className="form-group">
                  <label className="col-3">Title</label>
                  <div className="col-9">
                    <input type="text" name="title" className="form-control" />
                  </div>
              </div>
              <div className="form-group">
                  <label className="col-3">Owner Name</label>
                  <div className="col-9">
                    <input type="text" name="ownerName" className="form-control" />
                  </div>
              </div>
              <div className="form-group">
                  <label className="col-3">Mobile No</label>
                  <div className="col-9">
                    <input type="text" name="mobileNo" className="form-control" />
                  </div>
              </div>
              <div className="form-group">
                  <label className="col-3">Address</label>
                  <div className="col-9">
                    <input type="text" name="address" className="form-control" />
                  </div>
              </div>
          </div>
        </Modal.Body>
        <Modal.Footer className="justify-content-end">
          <button className="btn btn-success " onClick={props.onHide}><i className="fas fa-check"></i> Add</button>
        </Modal.Footer>
      </Modal>
    );
  }

  const Stores = (props) => {
    const [modalConfirmationShow, setModalConfirmationShow] = React.useState(false);
    const [modalStoreShow, setModalStoreShow] = React.useState(false);
    const [confirmationObj, setConfirmationObj] = React.useState({});

    const setConfirmationData = (type) => {
        setModalConfirmationShow(true);
        setConfirmationObj(constant.CONFIRMATION_MSG.store[type]);
    };


    return (
        <div className="container-fluid">
            <div className="content-max">
                <div className="card">
                    <div className="card-header">
                        <div className="d-flex align-items-center justify-content-between">
                            <h3><i className="fas fa-store"></i> Stores</h3>
                            <div className="actions">
                                <button className="btn btn-sm btn-secondary" onClick={() => setModalStoreShow(true)}><i className="fas fa-plus"></i> Add Store</button>
                            </div>
                        </div>
                    </div>
                    <div className="card-body p-0">
                        <div className="table">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Owner Name</th>
                                        <th>Mobile Number</th>
                                        <th>Address</th>
                                        <th>Status</th>
                                        <th className="text-right">Created At</th>
                                        <th className="text-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><NavLink to={`/admin/stores/testId`}>Hariom Hair style</NavLink></td>
                                        <td><NavLink to={`/admin/stores/testId`}>Jignesh Vanand</NavLink></td>
                                        <td><NavLink to={`/admin/stores/testId`}>9584756892</NavLink></td>
                                        <td>12, Manglam soc, Arbudanagar, Odhav, A'bad - 382415 </td>
                                        <td className="text-success">Active</td>
                                        <td className="text-right">03-08-2019 05:00 PM</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-danger btn-outline mr-1" title="Ban Store" onClick={() => setConfirmationData('deactive')}><i className="fas fa-ban"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit Store" onClick={() => setModalStoreShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete Store" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><NavLink to={`/admin/stores/testId`}>Chamunda Hair style</NavLink></td>
                                        <td><NavLink to={`/admin/stores/testId`}>Paresh Vanand</NavLink></td>
                                        <td><NavLink to={`/admin/stores/testId`}>9584756892</NavLink></td>
                                        <td>122, Arbudanagar, Odhav, A'bad - 382415 </td>
                                        <td className="text-danger">Deactive</td>
                                        <td className="text-right">03-08-2019 05:00 PM</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-success btn-outline mr-1" title="Active Store" onClick={() => setConfirmationData('active')}><i className="fas fa-check"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit Store" onClick={() => setModalStoreShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete Store" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><NavLink to={`/admin/stores/testId`}>Hariom Hair style</NavLink></td>
                                        <td><NavLink to={`/admin/stores/testId`}>Jignesh Vanand</NavLink></td>
                                        <td><NavLink to={`/admin/stores/testId`}>9584756892</NavLink></td>
                                        <td>12, Manglam soc, Arbudanagar, Odhav, A'bad - 382415 </td>
                                        <td className="text-success">Active</td>
                                        <td className="text-right">03-08-2019 05:00 PM</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-danger btn-outline mr-1" title="Ban Store" onClick={() => setConfirmationData('deactive')}><i className="fas fa-ban"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit Store" onClick={() => setModalStoreShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete Store" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><NavLink to={`/admin/stores/testId`}>Chamunda Hair style</NavLink></td>
                                        <td><NavLink to={`/admin/stores/testId`}>Paresh Vanand</NavLink></td>
                                        <td><NavLink to={`/admin/stores/testId`}>9584756892</NavLink></td>
                                        <td>122, Arbudanagar, Odhav, A'bad - 382415 </td>
                                        <td className="text-danger">Deactive</td>
                                        <td className="text-right">03-08-2019 05:00 PM</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-success btn-outline mr-1" title="Active Store" onClick={() => setConfirmationData('active')}><i className="fas fa-check"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit Store" onClick={() => setModalStoreShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete Store" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><NavLink to={`/admin/stores/testId`}>Hariom Hair style</NavLink></td>
                                        <td><NavLink to={`/admin/stores/testId`}>Jignesh Vanand</NavLink></td>
                                        <td><NavLink to={`/admin/stores/testId`}>9584756892</NavLink></td>
                                        <td>12, Manglam soc, Arbudanagar, Odhav, A'bad - 382415 </td>
                                        <td className="text-success">Active</td>
                                        <td className="text-right">03-08-2019 05:00 PM</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-danger btn-outline mr-1" title="Ban Store" onClick={() => setConfirmationData('deactive')}><i className="fas fa-ban"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit Store" onClick={() => setModalStoreShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete Store" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><NavLink to={`/admin/stores/testId`}>Chamunda Hair style</NavLink></td>
                                        <td><NavLink to={`/admin/stores/testId`}>Paresh Vanand</NavLink></td>
                                        <td><NavLink to={`/admin/stores/testId`}>9584756892</NavLink></td>
                                        <td>122, Arbudanagar, Odhav, A'bad - 382415 </td>
                                        <td className="text-danger">Deactive</td>
                                        <td className="text-right">03-08-2019 05:00 PM</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-success btn-outline mr-1" title="Active Store" onClick={() => setConfirmationData('active')}><i className="fas fa-check"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit Store" onClick={() => setModalStoreShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete Store" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><NavLink to={`/admin/stores/testId`}>Hariom Hair style</NavLink></td>
                                        <td><NavLink to={`/admin/stores/testId`}>Jignesh Vanand</NavLink></td>
                                        <td><NavLink to={`/admin/stores/testId`}>9584756892</NavLink></td>
                                        <td>12, Manglam soc, Arbudanagar, Odhav, A'bad - 382415 </td>
                                        <td className="text-success">Active</td>
                                        <td className="text-right">03-08-2019 05:00 PM</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-danger btn-outline mr-1" title="Ban Store" onClick={() => setConfirmationData('deactive')}><i className="fas fa-ban"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit Store" onClick={() => setModalStoreShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete Store" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><NavLink to={`/admin/stores/testId`}>Chamunda Hair style</NavLink></td>
                                        <td><NavLink to={`/admin/stores/testId`}>Paresh Vanand</NavLink></td>
                                        <td><NavLink to={`/admin/stores/testId`}>9584756892</NavLink></td>
                                        <td>122, Arbudanagar, Odhav, A'bad - 382415 </td>
                                        <td className="text-danger">Deactive</td>
                                        <td className="text-right">03-08-2019 05:00 PM</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-success btn-outline mr-1" title="Active Store" onClick={() => setConfirmationData('active')}><i className="fas fa-check"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit Store" onClick={() => setModalStoreShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete Store" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><NavLink to={`/admin/stores/testId`}>Hariom Hair style</NavLink></td>
                                        <td><NavLink to={`/admin/stores/testId`}>Jignesh Vanand</NavLink></td>
                                        <td><NavLink to={`/admin/stores/testId`}>9584756892</NavLink></td>
                                        <td>12, Manglam soc, Arbudanagar, Odhav, A'bad - 382415 </td>
                                        <td className="text-success">Active</td>
                                        <td className="text-right">03-08-2019 05:00 PM</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-danger btn-outline mr-1" title="Ban Store" onClick={() => setConfirmationData('deactive')}><i className="fas fa-ban"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit Store" onClick={() => setModalStoreShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete Store" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><NavLink to={`/admin/stores/testId`}>Chamunda Hair style</NavLink></td>
                                        <td><NavLink to={`/admin/stores/testId`}>Paresh Vanand</NavLink></td>
                                        <td><NavLink to={`/admin/stores/testId`}>9584756892</NavLink></td>
                                        <td>122, Arbudanagar, Odhav, A'bad - 382415 </td>
                                        <td className="text-danger">Deactive</td>
                                        <td className="text-right">03-08-2019 05:00 PM</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-success btn-outline mr-1" title="Active Store" onClick={() => setConfirmationData('active')}><i className="fas fa-check"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit Store" onClick={() => setModalStoreShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete Store" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="card-footer">
                        <div className="d-flex align-items-center justify-content-between">
                            <span>1 - 10 from 36 results</span>
                            <div className="d-flex">
                                <button className="btn btn-sm btn-grey btn-outline mr-1">Prev</button>
                                <button className="btn btn-sm btn-grey btn-outline">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
                <Confirmation show={modalConfirmationShow} data={confirmationObj} onHide={() => setModalConfirmationShow(false)} />
                <AddEditStoreModal show={modalStoreShow} onHide={() => setModalStoreShow(false)} />
            
            </div>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        loggedInUser: '',
    }
};
const mapDispatchToProps = dispatch => {
    return {
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Stores);

