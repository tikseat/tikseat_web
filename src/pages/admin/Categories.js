import React from 'react';
import { connect } from 'react-redux';
import Confirmation from '../shared/modals/Confirmation';
import AddEditCategoryModal from '../shared/modals/AddEditCategory';
import * as constant from '../../core/constant';

const Categories = (props) => {
    const [modalConfirmationShow, setModalConfirmationShow] = React.useState(false);
    const [modalCategoryShow, setModalCategoryShow] = React.useState(false);
    const [confirmationObj, setConfirmationObj] = React.useState({});

    const setConfirmationData = (type) => {
        setModalConfirmationShow(true);
        setConfirmationObj(constant.CONFIRMATION_MSG.category[type]);
    };

    return (
        <div className="container-fluid">
            <div className="content-max">
                <div className="card">
                    <div className="card-header">
                        <div className="d-flex align-items-center justify-content-between">
                            <h3><i className="fas fa-tags"></i> Categories</h3>
                            <div className="actions">
                                <button className="btn btn-sm btn-secondary" onClick={() => setModalCategoryShow(true)}><i className="fas fa-plus"></i> Add Category</button>
                            </div>
                        </div>
                    </div>
                    <div className="card-body p-0">
                        <div className="table">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                        <th className="text-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Gents </td>
                                        <td>Main</td>
                                        <td className="text-success">Active</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-danger btn-outline mr-1" title="Deactive Category" onClick={() => setConfirmationData('deactive')}><i className="fas fa-ban"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit Category" onClick={() => setModalCategoryShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete Category" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Ladies </td>
                                        <td>Main</td>
                                        <td className="text-danger">Deactive</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-success btn-outline mr-1" title="Active User" onClick={() => setConfirmationData('active')}><i className="fas fa-check"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit Category" onClick={() => setModalCategoryShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete Category" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Gents </td>
                                        <td>Main</td>
                                        <td className="text-success">Active</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-danger btn-outline mr-1" title="Deactive Category" onClick={() => setConfirmationData('deactive')}><i className="fas fa-ban"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit Category" onClick={() => setModalCategoryShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete Category" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Ladies </td>
                                        <td>Main</td>
                                        <td className="text-danger">Deactive</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-success btn-outline mr-1" title="Active User" onClick={() => setConfirmationData('active')}><i className="fas fa-check"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit Category" onClick={() => setModalCategoryShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete Category" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Gents </td>
                                        <td>Main</td>
                                        <td className="text-success">Active</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-danger btn-outline mr-1" title="Deactive Category" onClick={() => setConfirmationData('deactive')}><i className="fas fa-ban"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit Category" onClick={() => setModalCategoryShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete Category" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Ladies </td>
                                        <td>Main</td>
                                        <td className="text-danger">Deactive</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-success btn-outline mr-1" title="Active User" onClick={() => setConfirmationData('active')}><i className="fas fa-check"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit Category" onClick={() => setModalCategoryShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete Category" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Gents </td>
                                        <td>Main</td>
                                        <td className="text-success">Active</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-danger btn-outline mr-1" title="Deactive Category" onClick={() => setConfirmationData('deactive')}><i className="fas fa-ban"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit Category" onClick={() => setModalCategoryShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete Category" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Ladies </td>
                                        <td>Main</td>
                                        <td className="text-danger">Deactive</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-success btn-outline mr-1" title="Active User" onClick={() => setConfirmationData('active')}><i className="fas fa-check"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit Category" onClick={() => setModalCategoryShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete Category" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Gents </td>
                                        <td>Main</td>
                                        <td className="text-success">Active</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-danger btn-outline mr-1" title="Deactive Category" onClick={() => setConfirmationData('deactive')}><i className="fas fa-ban"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit Category" onClick={() => setModalCategoryShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete Category" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Ladies </td>
                                        <td>Main</td>
                                        <td className="text-danger">Deactive</td>
                                        <td className="text-right">
                                            <button className="btn btn-sm btn-success btn-outline mr-1" title="Active User" onClick={() => setConfirmationData('active')}><i className="fas fa-check"></i></button>
                                            <button className="btn btn-sm btn-third btn-outline mr-1" title="Edit Category" onClick={() => setModalCategoryShow(true)}><i className="fas fa-pencil-alt"></i></button>
                                            <button className="btn btn-sm btn-danger btn-outline" title="Delete Category" onClick={() => setConfirmationData('delete')}><i className="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="card-footer">
                        <div className="d-flex align-items-center justify-content-between">
                            <span>1 - 10 from 36 results</span>
                            <div className="d-flex">
                                <button className="btn btn-sm btn-grey btn-outline mr-1">Prev</button>
                                <button className="btn btn-sm btn-grey btn-outline">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Confirmation show={modalConfirmationShow} data={confirmationObj} onHide={() => setModalConfirmationShow(false)} />
            <AddEditCategoryModal show={modalCategoryShow} onHide={() => setModalCategoryShow(false)} />
        </div>
    );
}

const mapStateToProps = state => {
    return {
        loggedInUser: '',
    }
};
const mapDispatchToProps = dispatch => {
    return {
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Categories);

