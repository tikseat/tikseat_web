import React from 'react';
import Modal from 'react-bootstrap/Modal';

const AddEditCategoryModal = (props) => {
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Add Category
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="form-container">
              <div className="form-group">
                  <label className="col-3">Name</label>
                  <div className="col-9">
                    <input type="text" name="name" className="form-control" />
                  </div>
              </div>
              <div className="form-group">
                  <label className="col-3">Type</label>
                  <div className="col-9">
                    <select type="text" name="type" className="form-control">
                        <option value="M">Main</option>
                        <option value="S">Service</option>
                    </select>
                  </div>
              </div>
          </div>
        </Modal.Body>
        <Modal.Footer className="justify-content-end">
          <button className="btn btn-success " onClick={props.onHide}><i className="fas fa-check"></i> Add</button>
        </Modal.Footer>
      </Modal>
    );
}

export default AddEditCategoryModal;