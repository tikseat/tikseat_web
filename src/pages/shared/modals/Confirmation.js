import React from 'react';
import Modal from 'react-bootstrap/Modal';

const Confirmation = (props) => {
    console.log('object', props);
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Confirmation
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p className="mb-0 text-center">{props.data.message}</p>
        </Modal.Body>
        <Modal.Footer className="justify-content-center">
          <button className="btn btn-success " onClick={props.onHide}><i className="fas fa-check"></i> {props.data.okBtnTxt}</button>
          <button className="btn btn-danger" onClick={props.onHide}><i className="fas fa-times"></i> {props.data.cancelBtnTxt}</button>
        </Modal.Footer>
      </Modal>
    );
}

export default Confirmation;