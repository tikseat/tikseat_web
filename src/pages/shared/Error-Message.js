import React, { useEffect } from 'react';

const ErrorMessage = (props) => {

    return (
        <>
            {props && props.message ?
                <div>
                    <span>{props.message}</span>
                </div>
                : null}
        </>
    )
}

export default ErrorMessage;
