import React, { useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

const BasicInformation = (props) => {

    return (
        <div className="container-fluid">
            <div className="content-max">
            <div className="card">
                <div className="card-header">
                    <div className="d-flex align-items-center justify-content-between">
                        <h3><i className="fas fa-info-circle"></i> Basic Information</h3>
                    </div>
                </div>
                <div className="card-body p-0"></div>
                <div className="card-footer"></div>
                </div>
            </div>
        </div>
    )
}

export default BasicInformation;
