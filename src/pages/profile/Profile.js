import React, { useState } from 'react';
import Formsy from 'formsy-react';
import MyInput from '../../shared/MyInput';
import MyInputMask from '../../shared/MyInputMask';
import ErrorHandel from '../../shared/ErrorHandel';
import { POST_UPDATEPROFILE_MUTATION } from '../../core/query-constant/profile';
import { mutate } from '../../core/apollo-service';
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';
import { refresh } from '../../core/auth';
import ButtonLoader from '../../shared/button-loader';

const Profile = (props) => {

    const [canSubmit, setCanSubmit] = useState(false);
    const [userProfileForm, setUserProfileForm] = useState('');
    const [isbuttonLoading, setIsButtonLoading] = useState(false);


    const submit = (model) => {
        setIsButtonLoading(true);
        mutate(POST_UPDATEPROFILE_MUTATION, model).then((response) => {
            setIsButtonLoading(false);
            if (response.myprofile && response.myprofile.updateProfile && response.myprofile.updateProfile.record) {
                refresh();
                props.history.push('/dashboard');
            }
        }, error => {
            setIsButtonLoading(false);
        });
    }
    const { loggedInUser } = props;
    return (

        <div>
            <h1>My Profile</h1>
            <Formsy ref={(formRef) => { setUserProfileForm(formRef) }} onValidSubmit={submit} onValid={() => setCanSubmit(true)} onInvalid={() => setCanSubmit(false)}>
                <ErrorHandel form={userProfileForm} />
                <MyInput
                    name="firstName"
                    type="text"
                    label="First Name"
                    id="firstName"
                    placeholder="First Name"
                    value={loggedInUser.firstName ? loggedInUser.firstName : ''}
                    className="form-control"
                    required
                />
                <MyInput
                    name="lastName"
                    type="text"
                    label="Last Name"
                    id="lastName"
                    placeholder="Last Name"
                    value={loggedInUser.lastName ? loggedInUser.lastName : ''}
                    className="form-control"
                    required
                />
                <MyInput
                    name="email"
                    type="text"
                    label="E-mail"
                    id="email"
                    placeholder="E-mail"
                    validations="isEmail"
                    validationError="Invalid email address"
                    value={loggedInUser.email ? loggedInUser.email : ''}
                    className="form-control"
                    required
                />
                <MyInputMask
                    name="phone"
                    id="phone"
                    label="Phone"
                    mask="9999999999"
                    placeholder="Phone"
                    validations={{
                        minLength: 10
                    }}
                    validationError="Please enter a valid mobile number."
                    value={loggedInUser.phone ? loggedInUser.phone : ''}
                    className="form-control"
                />
                <button disabled={!canSubmit} type="submit">
                     <ButtonLoader isLoading={isbuttonLoading} label="submit" />
                </button>
            </Formsy>
        </div >
    )
}


const mapStateToProps = state => {
    return {
        loggedInUser: '',
    }
};

const mapDispatchToProps = dispatch => {
    return {
        login: (response) => dispatch(actionCreators.login(response))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);