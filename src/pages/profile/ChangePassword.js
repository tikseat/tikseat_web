import React, { useState, useEffect } from 'react';
import Formsy from 'formsy-react';
import MyInput from '../../shared/MyInput';
import ErrorHandel from '../../shared/ErrorHandel';
import { POST_CHANGEPASSWORD_MUTATION } from '../../core/query-constant/profile';
import { mutate } from '../../core/apollo-service';
import ButtonLoader from '../../shared/button-loader';

const ChangePassword = (props) => {

    const [canSubmit, setCanSubmit] = useState(false);
    const [isValueChangeError, setIsValueChangeError] = useState(false);
    const [isbuttonLoading, setIsButtonLoading] = useState(false);
    const [resetPasswordForm, setResetPasswordForm] = useState('');

    const submit = (model) => {
            setIsButtonLoading(true);
            mutate(POST_CHANGEPASSWORD_MUTATION, model).then((response) => {
                setIsButtonLoading(false);
                if (response.myprofile && response.myprofile.changePassword) {
                    props.history.push('/dashboard');
                }
            }, error => {
                setIsButtonLoading(false);
            });
    }

    const valueChanges = (value) => {
        const formValue = resetPasswordForm.getModel();
        if (formValue.password !== value) {
            setIsValueChangeError(true);
        }
    }

    useEffect(() => {
        if (isValueChangeError) {
            if (resetPasswordForm) {
                resetPasswordForm.updateInputsWithError({
                    cPassword: 'New password and confirm password does not match.'
                });
                setIsValueChangeError(false);
            }
        }
    }, [isValueChangeError]);

    return (
        <div>
            <h1>Reset Password</h1>
            <Formsy ref={(formRef) => { setResetPasswordForm(formRef) }} onValidSubmit={submit} onValid={() => setCanSubmit(true)} onInvalid={() => setCanSubmit(false)}>
                <ErrorHandel form={resetPasswordForm} />
                <MyInput
                    name="oPassword"
                    type="password"
                    label="Old Password"
                    id="oPassword"
                    placeholder="Old Password"
                    value={''}
                    className="form-control"
                    required
                />
                <MyInput
                    name="password"
                    type="password"
                    label="Password"
                    id="password"
                    placeholder="Password"
                    value={''}
                    className="form-control"
                    required
                />
                <MyInput
                    name="cPassword"
                    type="password"
                    label="Confirm Password"
                    valueChanges={(e) => valueChanges(e)}
                    id="cPassword"
                    placeholder="Confirm Password"
                    value={''}
                    className="form-control"
                    required
                />
                <button disabled={!canSubmit} type="submit">
                     <ButtonLoader isLoading={isbuttonLoading} label="submit" />
                </button>
            </Formsy>
        </div >
    )
}

export default ChangePassword;