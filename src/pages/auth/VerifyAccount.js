import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import ErrorHandel from '../../shared/ErrorHandel';
import '../../assets/scss/auth/auth.scss';
import lightLogo from '../../assets/img/logo.png';

const VerifyAccount = (props) => {
    const [apiResponse, setapiResponse] = useState('');
    useEffect(() => {
        // mutate(POST_ACTIVATION_MUTATION, { activationCode: props.match.params.token }).then((response) => {
        //     if (response.auth && response.auth.activationLink) {
        //         setapiResponse(response.auth.activationLink.message);
        //     }
        // })
    }, []);

    return (
        <div className="auth-wrapper">
            <div className="auth-container">
                <div className="left-content text-center d-block">
                    <div className="page-desc-container">
                        <div className="logo">
                            <Link to="/"><img src={lightLogo} alt="logo" width="250" /></Link>
                        </div>
                        <div className="page-content">
                            <h4>Verify Your Account</h4>
                            <ErrorHandel form={null} />
                            <p>{apiResponse}</p>
                            <br />
                            <Link to="/auth/login">Business Login</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default VerifyAccount;