import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom";
import Formsy from 'formsy-react';
import MyInput from '../../shared/MyInput';
import ErrorHandel from '../../shared/ErrorHandel';
import ButtonLoader from '../../shared/button-loader';
import '../../assets/scss/auth/auth.scss';
import lightLogo from '../../assets/img/logo.png';

const ResetPassword = (props) => {

    const [canSubmit, setCanSubmit] = useState(false);
    const [isValueChangeError, setIsValueChangeError] = useState(false);
    const [isbuttonLoading, setIsButtonLoading] = useState(false);
    const [resetPasswordForm, setResetPasswordForm] = useState('');

    const submit = (model) => {
        let payload = model;
        // payload.token = props.match.params.token;
        // setIsButtonLoading(true);
        // mutate(POST_RESETPASSWORD_MUTATION, payload).then((response) => {
        //     setIsButtonLoading(false);
        //     if (response.auth && response.auth.updatePassword) {
        //         props.history.push('/auth/login');
        //     }
        // }, error => {
        //     setIsButtonLoading(false);
        // });
    }

    const valueChanges = (value) => {
        const formValue = resetPasswordForm.getModel();
        if (formValue.password !== value) {
            setIsValueChangeError(true);
        }
    }

    useEffect(() => {
        if (isValueChangeError) {
            if (resetPasswordForm) {
                resetPasswordForm.updateInputsWithError({
                    cPassword: 'New password and confirm password does not match.'
                });
                setIsValueChangeError(false);
            }
        }
    }, [isValueChangeError]);

    return (
        <div className="auth-wrapper">
            <div className="auth-container">
                <div className="left-content text-center">
                    <div className="page-desc-container">
                        <div className="logo">
                            <Link to="/"><img src={lightLogo} alt="logo" width="250" /></Link>
                        </div>
                        <div className="page-content">
                            <h4>Content Title Here..</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <br />
                            <h4>Content Title Here..</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <br />
                            <h4>Content Title Here..</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <br />
                            <h4>Content Title Here..</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <br />
                            <h4>Content Title Here..</h4>
                            <p className="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                    </div>
                </div>
                <div className="right-content">
                    <div className="form-container">
                        <div className="title-container">
                            <h3>Reset Password</h3>
                            <h4>Lorem Ipsum text here Lorem Ipsum text here Lorem Ipsum</h4>
                        </div>
                        <Formsy ref={(formRef) => { setResetPasswordForm(formRef) }} onValidSubmit={submit} onValid={() => setCanSubmit(true)} onInvalid={() => setCanSubmit(false)}>
                            <ErrorHandel form={resetPasswordForm} />
                            <MyInput
                                name="password"
                                type="password"
                                label="Password"
                                id="password"
                                placeholder="Enter Password"
                                value={''}
                                className="form-control"
                                groupClassName="form-group"
                                required
                            />
                            <MyInput
                                name="cPassword"
                                type="password"
                                label="Confirm Password"
                                valueChanges={(e) => valueChanges(e)}
                                id="cPassword"
                                placeholder="Enter Confirm Password"
                                value={''}
                                className="form-control"
                                groupClassName="form-group"
                                required
                            />
                            <button className="btn login-btn" disabled={!canSubmit} type="submit">
                                <ButtonLoader isLoading={isbuttonLoading} label="Reset Password" />
                            </button>
                        </Formsy>
                        <div className="signup-link mt-5">
                            <Link className="d-block" to="/auth/login">Business Login</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ResetPassword;