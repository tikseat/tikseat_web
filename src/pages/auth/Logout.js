import React, { useEffect } from 'react';
import { connect } from "react-redux";
import { Redirect } from 'react-router';
import * as actionCreators from '../../store/actions';

const Logout = (props) => {
    useEffect(() => {
        props.doLogout();
    }, []);
    return (
        <Redirect to="/" />
    );
}

const mapStateToProps = state => {
    return {
    }
};

const mapDispatchToProps = dispatch => {
    return {
        doLogout: () => dispatch(actionCreators.logout()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Logout);

