import React, { useState } from 'react';
import { Link } from "react-router-dom";
import Formsy from 'formsy-react';

import apiCall from './../../core/api-service';
import { setCredentials } from './../../core/auth'
import MyInput from '../../shared/MyInput';
import ErrorHandel from '../../shared/ErrorHandel';
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index'
import ButtonLoader from '../../shared/button-loader';
import '../../assets/scss/auth/auth.scss';
import lightLogo from '../../assets/img/logo.png';
import ErrorMessage from '../shared/Error-Message';

const Login = (props) => {

    const [canSubmit, setCanSubmit] = useState(false);
    const [isbuttonLoading, setIsButtonLoading] = useState(false);
    const [loginForm, setLoginForm] = useState('');
    const [loginError, setLoginError] = useState('');

    const submit = (formData) => {
        setIsButtonLoading(true);
        apiCall({
            method: 'post',
            url: '/auth/login',
            payload: formData
        }).then((response) => {
            console.log('res', response)
            response.userType = 'bussiness';
            // response.userType = 'admin';
            // response.userType = 'user';
            setIsButtonLoading(false);
            setCredentials(response);
            props.login(response);
            handleRedirection(response.userType)
        }, error => {
            console.log('error', error)
            setIsButtonLoading(false);
            setLoginError(error.message);
        })
    }

    const handleRedirection = (userType) => {
        switch (userType) {
            case 'bussiness':
                props.history.push('business/dashboard');
                break;
            case 'admin':
                props.history.push('admin/dashboard');
                break;
            case 'user':
                props.history.push('/');
                break;
        }
    }

    return (
        <div className="auth-wrapper">
            <div className="auth-container">
                <div className="left-content text-center">
                    <div className="page-desc-container">
                        <div className="logo">
                            <Link to="/"><img src={lightLogo} alt="logo" width="250" /></Link>
                        </div>
                        <div className="page-content">
                            <h4>Content Title Here..</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <br />
                            <h4>Content Title Here..</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <br />
                            <h4>Content Title Here..</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <br />
                            <h4>Content Title Here..</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <br />
                            <h4>Content Title Here..</h4>
                            <p className="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                    </div>
                </div>
                <div className="right-content">
                    <div className="form-container">
                        <div className="title-container">
                            <h3>Business Login</h3>
                            <h4>Lorem Ipsum text here Lorem Ipsum text here Lorem Ipsum</h4>
                        </div>
                        <Formsy ref={(formRef) => { setLoginForm(formRef) }} onValidSubmit={submit} onValid={() => setCanSubmit(true)} onInvalid={() => setCanSubmit(false)}>
                            <ErrorHandel form={loginForm} />
                            <MyInput
                                name="username"
                                type="text"
                                label="E-mail Address"
                                id="username"
                                placeholder="Enter E-mail Address"
                                validations="isEmail"
                                validationError="Invalid email address"
                                className="form-control"
                                groupClassName="form-group"
                                required
                            />
                            <MyInput
                                name="password"
                                type="password"
                                label="Password"
                                id="password"
                                placeholder="Enter Password"
                                className="form-control"
                                groupClassName="form-group mb-2"
                                required
                            />
                            <div className="text-right">
                                <Link to="/auth/forgot-password">forgot password?</Link>
                            </div>
                            <button className="btn login-btn" disabled={!canSubmit} type="submit">
                                <ButtonLoader isLoading={isbuttonLoading} label="Login" />
                            </button>
                        </Formsy>
                        <div className="signup-link mt-5">
                            <ErrorMessage message={loginError} />
                        </div>
                        <div className="signup-link mt-5">
                            Want your business growth free ? <Link className="d-block" to="/auth/signup">Create Your Own Business</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return {
    }
};

const mapDispatchToProps = dispatch => {
    return {
        login: (response) => dispatch(actionCreators.login(response))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);