import React, { useState } from 'react';
import { Link } from "react-router-dom";
import Formsy from 'formsy-react';
import MyInput from '../../shared/MyInput';
import ErrorHandel from '../../shared/ErrorHandel';
import ButtonLoader from '../../shared/button-loader';
import '../../assets/scss/auth/auth.scss';
import lightLogo from '../../assets/img/logo.png';

const ForgotPassword = (props) => {

    const [canSubmit, setCanSubmit] = useState(false);
    const [isbuttonLoading, setIsButtonLoading] = useState(false);
    const [forgotPasswordForm, setforgotPasswordFormForm] = useState('');

    const submit = (model) => {
            // setIsButtonLoading(true);
            // mutate(POST_FORGOTPASSWORD_MUTATION, model).then((response) => {
            //     setIsButtonLoading(false);
            //     if (response.auth && response.auth.forgotPassword) {
            //         // props.history.push('/auth/login');
            //     }
            // }, error => {
            //     setIsButtonLoading(false);
            // });
    }

    // console.log("componentWillReceiveProps");


    return (
        <div className="auth-wrapper">
            <div className="auth-container">
                <div className="left-content text-center">
                    <div className="page-desc-container">
                        <div className="logo">
                            <Link to="/"><img src={lightLogo} alt="logo" width="250" /></Link>
                        </div>
                        <div className="page-content">
                            <h4>Content Title Here..</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <br />
                            <h4>Content Title Here..</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <br />
                            <h4>Content Title Here..</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <br />
                            <h4>Content Title Here..</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <br />
                            <h4>Content Title Here..</h4>
                            <p className="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                    </div>
                </div>
                <div className="right-content">
                    <div className="form-container">
                        <div className="title-container">
                            <h3>Forgot Password</h3>
                            <h4>Lorem Ipsum text here Lorem Ipsum text here Lorem Ipsum</h4>
                        </div>
                        <Formsy ref={(formRef) => { setforgotPasswordFormForm(formRef) }} onValidSubmit={submit} onValid={() => setCanSubmit(true)} onInvalid={() => setCanSubmit(false)}>
                            <ErrorHandel form={forgotPasswordForm} />
                            <MyInput
                                name="email"
                                type="text"
                                label="E-mail Address"
                                id="email"
                                placeholder="Enter E-mail Address"
                                validations="isEmail"
                                validationError="Invalid email address"
                                value={''}
                                className="form-control"
                                groupClassName="form-group mb-2"
                                required
                            />
                            <div className="text-right">
                                <Link to="/auth/login">Back to Login?</Link>
                            </div>
                            <button className="btn login-btn" disabled={!canSubmit} type="submit">
                                <ButtonLoader isLoading={isbuttonLoading} label="Send" />
                            </button>
                        </Formsy>
                        <div className="signup-link mt-5">
                        Want your business growth free ? <Link className="d-block" to="/auth/signup">Create Own Business</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ForgotPassword;