import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom";
import Formsy from 'formsy-react';

import apiCall from './../../core/api-service';
import MyInput from '../../shared/MyInput';
import MyInputMask from '../../shared/MyInputMask';
import ErrorHandel from '../../shared/ErrorHandel';
import ButtonLoader from '../../shared/button-loader';
import '../../assets/scss/auth/auth.scss';
import lightLogo from '../../assets/img/logo.png';

const Signup = (props) => {

    const [canSubmit, setCanSubmit] = useState(false);
    const [isbuttonLoading, setIsButtonLoading] = useState(false);
    const [signupForm, setSignupForm] = useState('');

    const submit = (formData) => {
       let tempPayload = {
            firstName: formData.ownerName,
            lastName: formData.storeTitle,
            email: formData.email,
            phone: formData.mobileNo,
            password: formData.password,
            cPassword: formData.cPassword
          }
        setIsButtonLoading(true);
        apiCall({
            method: 'post',
            url: '/auth/signup',
            payload: tempPayload
        }).then((response) => {
            if(response && response._id)
            setIsButtonLoading(false);
            props.history.push('/auth/login');
        }, error => {
            setIsButtonLoading(false);
            console.log('error', error)
        })
    }


    return (

        <div className="auth-wrapper">
            <div className="auth-container">
                <div className="left-content text-center">
                    <div className="page-desc-container">
                        <div className="logo">
                            <Link to="/"><img src={lightLogo} alt="logo" width="250" /></Link>
                        </div>
                        <div className="page-content">
                            <h4>Content Title Here..</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <br />
                            <h4>Content Title Here..</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <br />
                            <h4>Content Title Here..</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <br />
                            <h4>Content Title Here..</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <br />
                            <h4>Content Title Here..</h4>
                            <p className="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                    </div>
                </div>
                <div className="right-content">
                    <div className="form-container">
                        <div className="title-container">
                            <h3>Create Your Own Business</h3>
                            <h4>Lorem Ipsum text here Lorem Ipsum text here Lorem Ipsum</h4>
                        </div>
                        <Formsy ref={(formRef) => { setSignupForm(formRef) }} onValidSubmit={submit} onValid={() => setCanSubmit(true)} onInvalid={() => setCanSubmit(false)}>
                            <ErrorHandel form={signupForm} />
                            <MyInput
                                name="storeTitle"
                                type="text"
                                label="Store Title"
                                id="storeTitle"
                                placeholder="Enter Store Title"
                                className="form-control"
                                groupClassName="form-group"
                                required
                            />
                            <MyInput
                                name="ownerName"
                                type="text"
                                label="Owner Name"
                                id="ownerName"
                                placeholder="Enter Owner Name"
                                className="form-control"
                                groupClassName="form-group"
                                required
                            />
                            <MyInputMask
                                name="mobileNo"
                                id="mobileNo"
                                mask="9999999999"
                                label="Mobile No. (Whatsapp No)"
                                placeholder="Enter Mobile Number"
                                validations={{
                                    minLength: 10
                                }}
                                validationError="Please enter a valid mobile number."
                                className="form-control"
                                groupClassName="form-group"
                            />
                            <MyInput
                                name="email"
                                type="text"
                                label="E-mail Address"
                                id="email"
                                placeholder="Enter E-mail Address"
                                validations="isEmail"
                                validationError="Invalid email address"
                                className="form-control"
                                groupClassName="form-group"
                                required
                            />
                            
                            <MyInput
                                name="password"
                                type="password"
                                label="Password"
                                id="password"
                                placeholder="Enter Password"
                                className="form-control"
                                groupClassName="form-group"
                                required
                            />
                            <MyInput
                                name="cPassword"
                                type="password"
                                label="Confirm Password"
                                id="cPassword"
                                placeholder="Enter Confirm Password"
                                className="form-control"
                                groupClassName="form-group"
                                validations="equalsField:password"
                                validationError="Password and password confimation do not match."
                                required
                            />
                            <button className="btn login-btn" disabled={!canSubmit} type="submit">
                                <ButtonLoader isLoading={isbuttonLoading} label="Create Business" />
                            </button>
                        </Formsy>
                        <div className="signup-link mt-5">
                        Already Created Business ? <Link className="d-block" to="/auth/login">Business Login</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Signup;