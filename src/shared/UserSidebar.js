import React from 'react';
import { NavLink } from 'react-router-dom';
import routes from './../core/routes';


const UserSidebar = () => (
    <div className="sidebar d-xs-block d-sm-block d-md-none d-lg-block">
        <div className="logo-icon">
            <i className="fas fa-spinner"></i>
        </div>
        <div className="list-group list-group-flush">
            {routes
                .filter((prop) => prop.layout === "user" && prop.isSidebarMenu)
                .map((prop, key) => {
                    return (
                        <NavLink
                            to={{
                                pathname: prop.path,
                                state: { title: prop.name }
                            }}
                            key={key}
                            className="list-group-item list-group-item-action"
                            activeClassName="active"
                        >
                            <i className={prop.icon}></i>{prop.name}
                        </NavLink>
                    );
                })}
        </div>
    </div>
);

export default UserSidebar;
