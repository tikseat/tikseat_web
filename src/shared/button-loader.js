import React from 'react';

const ButtonLoader = (props) => {
    return (
        <div>
            {props && props.isLoading &&
                <div style={{ height: 20, width : 20 }}  className="spinner-border text-dark">
                    <span className="sr-only">Loading...</span>
                </div>
            }
            <span className="ml-2">{props.label} {props.isLoading}</span>
        </div>
    )
}
export default ButtonLoader;