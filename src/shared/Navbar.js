import React from 'react';
import { NavLink } from 'react-router-dom';

const toggleMenu = (event) => {
    event.preventDefault();
    let sidebarElement = document.getElementsByClassName("sidebar");
    let classList = sidebarElement[0].classList;
    classList.remove("d-md-none")
    classList.add("d-md-block")
    document.getElementById("wrapper").classList.toggle("toggled");
}

const Navbar = () => (
    <nav className="navbar navbar-expand-md navbar-light">
        <button onClick={(e) => toggleMenu(e)} className="btn btn-primary navbar-right" id="menu-toggle">
            <i className="fas fa-bars"></i>
        </button>
    </nav>
);

export default Navbar;
