
import { withFormsy } from 'formsy-react';
import React, { Component } from 'react';

const MyCheckbox = (props) => {
  const changeValue = (event) => {
    props.setValue(event.currentTarget.checked);
  }
  const errorMessage = props.getErrorMessage();
  const { id, name, className, labelClass, label } = props;

  return (
    <>
      <input
        id={id}
        className={className}
        type="checkbox"
        name={name}
        onChange={changeValue}
        // checked={this.props.value}
        checked={props.getValue() || ''}
      />
      <label htmlFor={id} className={labelClass}>
        {label}
      </label>
      <small className="error-text">{errorMessage}</small>
    </>
  );

}

export default withFormsy(MyCheckbox);