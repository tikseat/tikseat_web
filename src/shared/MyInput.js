import { withFormsy } from 'formsy-react';
import React from 'react';

const MyInput = (props) => {
  const changeValue = (event) => {
    props.setValue(event.currentTarget.value);
    if (props.valueChanges) {
      props.valueChanges(event.currentTarget.value)
    }
  }

  const errorMessage = props.getErrorMessage();
  const { id, name, className, type, disabled, label, placeholder, groupClassName } = props;
  return (
    <div className={groupClassName}>
      <label>{label} {props.isRequired() ? '*' : null}</label>
      <input
        onChange={changeValue}
        id={id}
        name={name}
        className={className}
        type={type ? type : 'text'}
        value={props.getValue() || ''}
        disabled={disabled ? disabled : false}
        placeholder={placeholder}
      />
      <small style={{ color: 'red' }}>{errorMessage}</small>
    </div>
  );

}

export default withFormsy(MyInput);
