import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import * as actionCreators from './../store/actions';
import { inArray } from '../core/util';

const ErrorHandel = (nextProps) => {
  const [header, setHeader] = useState('');
  useEffect(() => {
    if (!nextProps.isShowErrors) {
      setHeader('');
      if (nextProps.serverErrors && nextProps.serverErrors.length && nextProps.serverErrors[0].data) {
        nextProps.errorShow();
        Object.keys(nextProps.serverErrors[0].data).forEach((key) => {
          if (nextProps.form) {
            const keyArr = Object.keys(nextProps.form.getModel());
            if (inArray(key, keyArr)) {
              nextProps.form.updateInputsWithError({
                [`${key}`]: nextProps.serverErrors[0].data[key],
              });
            }
          }
          if (key === 'header') {
            setHeader(nextProps.serverErrors[0].data[key]);
          }
        });
      }
    }
  }, [nextProps]); // this line will tell react only trigger if count was changed
  return <small style={{ color: 'red' }}>{header}</small>;
};

const mapStateToProps = state => {
  return {
    serverErrors: state.auth.serverErrors,
    isShowErrors: state.auth.isShowErrors
  }
};

const mapDispatchToProps = dispatch => {
  return {
    errorShow: () => dispatch(actionCreators.errorShow()),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ErrorHandel);
