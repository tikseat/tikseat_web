import React from 'react';
import SearchBar from './SearchBar';


const Banner = (props) => {
    console.log(props, 'props');
    return (
        <div className="banner-container">
            <div className="container position-relative">
                {props.config.isSearchBarEnabled && 
                    <SearchBar />
                }
            </div>
        </div>
    )
}

export default Banner;
