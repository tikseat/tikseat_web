import React from 'react';
import '../../assets/scss/front/rating.scss';

const Rating = (props) => {
    return (
        <div className="rating-container d-flex justify-content-start">
            <span className="rating-box">3.5</span>
            <ul className="rating-stars-container d-flex justify-content-start align-items-center">
                <li><i className="fas fa-star"></i></li>
                <li><i className="fas fa-star"></i></li>
                <li><i className="fas fa-star"></i></li>
                <li><i className="fas fa-star-half-alt"></i></li>
                <li><i className="far fa-star"></i></li>
            </ul>
            <span className="reviews-label">150 reviews</span>
        </div>
    )
}

export default Rating;
