import React from 'react';
import '../../assets/scss/front/store-single.scss';
import storeImage1 from '../../assets/img/store/image1.jpg';
import Rating from './Rating';
import { Link } from 'react-router-dom';


const StoreSingle = (props) => {
    console.log(props, 'props');
    let storeClassName = 'store-single';
    if(props.config.isPopularStoreView){
        storeClassName = `store-single popular-store-single`
    }
    return (
        <div className={storeClassName}>
            <div className="d-flex justify-content-start">
                <div className="store-image">
                    <img src={storeImage1} width="100%" alt="store" />
                    { (!props.config.isPopularStoreView) && 
                    <div className="functionality-list d-flex align-items-center justify-content-center">
                        <span><i className="fas fa-male"></i></span>
                        <span className="off"><i className="fas fa-female"></i></span>
                        <span><i className="fas fa-wifi"></i></span>
                        <span className="off"><i className="fas fa-car"></i></span>
                        <span>A/C</span>
                    </div>
                    }
                </div>
                <div className="store-details-content">
                    <Link to="/store"><h3 className="store-title">Shree Chamunda Hair Style</h3></Link>
                    <Rating />
                    <div className="store-main-details">
                        <div className="detail-single">
                            <i className="fas fa-phone rotate-phone-icon"></i>
                            <p><b>9727190205</b> | Jignesh Patel</p>
                        </div>
                        <div className="detail-single">
                            <i className="fas fa-map-marker-alt"></i>
                            <p>
                            3, Manglam Society, Arbudanagar Road | More...</p>
                        </div>
                        <div className="detail-single">
                            <i className="fas fa-clock"></i>
                            <p>09:00 AM to 10:00 PM <br/> <span>MON,</span> <span className="off">TUE,</span> <span>WED,</span> <span>THU,</span> <span>FRI,</span> <span>SAT,</span> <span>SUN</span></p>
                        </div>
                        { (!props.config.isPopularStoreView) && 
                            <div className="detail-single">
                                <i className="fas fa-tags"></i>
                                <p>Hair Saloon, Nail Spa, Body art</p>
                            </div>
                        }
                    </div>
                </div>
                {(!props.config.isPopularStoreView) && 
                    <div className="view-details-content d-flex justify-content-center align-items-center">
                        <Link to="/store"><button className="btn btn-secondary">View Details</button></Link>
                    </div>
                }
           </div>
        </div>
    )
}

export default StoreSingle;
