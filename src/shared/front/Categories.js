import React from 'react';

const Categories = (props) => {
    return (
        <div className="category-list">
            <ul>
                <li>Hair Saloon</li>
                <li>Nail Spa</li>
                <li>Brows and lashes</li>
                <li>Hair Stylists</li>
                <li>Spa and massage</li>
                <li>Body art</li>
                <li>Bridal makeup</li>
                <li>Ladies beauty parlors</li>
                <li>Other</li>
            </ul>
        </div>
    )
}

export default Categories;
