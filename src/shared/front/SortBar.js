import React from 'react';

const SortBar = () => (
    <div className="sort-list">
        <ul className="d-flex align-items-center justify-content-between">
            <li className="active">Sort Results By :</li>
            <li>Top Results</li>
            <li>Populararity</li>
            <li>Location</li>
            <li>Distance <i className="fas fa-arrow-down"></i></li>
            <li>Ratings <i className="fas fa-arrow-up"></i></li>
        </ul>
    </div>
);

export default SortBar;
