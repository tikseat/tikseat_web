import React from 'react';
import '../../assets/scss/front/professional-single.scss';
import professionalImage1 from '../../assets/img/professional/image1.jpg';
import portfolioImage1 from '../../assets/img/portfolio/image1.jpg';
import portfolioImage2 from '../../assets/img/portfolio/image2.jpg';
import portfolioImage3 from '../../assets/img/portfolio/image3.jpg';
import portfolioImage4 from '../../assets/img/portfolio/image4.jpg';
import Rating from './Rating';
import { Link } from 'react-router-dom';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';


const ProfessionalSingle = (props) => {
    console.log(props, 'props');
    const [isFavorite, setIsFavorite] = React.useState(false);
    let professionalClassName = 'professional-single';
    if(props.config.isPopularProfessionalView){
        professionalClassName = `professional-single popular-professional-single`
    }
    return (
        <div className={professionalClassName}>
            <div className="d-flex justify-content-start align-items-center">
                <div className="professional-image">
                    <img src={professionalImage1} width="100%" alt="professional" />
                </div>
                <div className="professional-details-content">
                    <Link to="/store"><h3 className="professional-title">Jignesh Patel</h3></Link>
                    <Rating />
                    <div className="professional-main-details">
                        <div className="detail-single">
                            <i className="fas fa-phone rotate-phone-icon"></i>
                            <p><b>9727190205</b></p>
                        </div>
                        <div className="detail-single">
                            <i className="fas fa-map-marker-alt"></i>
                            <p>
                            3, Manglam Society, Arbudanagar Road | More...</p>
                        </div>
                        <div className="detail-single">
                            <i className="far fa-calendar-check"></i>
                            <p><span>Available Now</span></p>
                        </div>
                        { (!props.config.isPopularProfessionalView) && 
                            <div className="detail-single">
                                <i className="fas fa-tags"></i>
                                <p>Hair Saloon, Nail Spa, Body art</p>
                            </div>
                        }
                    </div>
                </div>
                {(!props.config.isPopularProfessionalView) && 
                    <>
                    <div className="view-details-content text-center">
                        {/* <div className="portfolio-tiles">
                            <div className="d-flex justify-content-start align-items-center flex-wrap">
                                <div className="tiles-single">
                                    <img src={portfolioImage1} alt="portfolioImage1" />
                                </div>
                                <div className="tiles-single">
                                    <img src={portfolioImage2} alt="portfolioImage2" />
                                </div>
                                <div className="tiles-single">
                                    <img src={portfolioImage3} alt="portfolioImage3" />
                                </div>
                                <div className="tiles-single">
                                    <img src={portfolioImage4} alt="portfolioImage4" />
                                </div>
                                <div className="tiles-single">
                                    <img src={portfolioImage1} alt="portfolioImage1" />
                                </div>
                                <div className="tiles-single">
                                    <img src={portfolioImage2} alt="portfolioImage2" />
                                </div>
                                <div className="tiles-single">
                                    <img src={portfolioImage3} alt="portfolioImage3" />
                                </div>
                                <div className="tiles-single">
                                    <img src={portfolioImage4} alt="portfolioImage4" />
                                </div>
                            </div>
                        </div> */}
                        <button className="btn btn-secondary mt-3">View Details</button>
                    </div>

                    <div className="professional-actions">
                    <OverlayTrigger placement="left" overlay={<Tooltip id="tooltip-favorite">{(isFavorite) ? "Favorited" : 'Favorite'}</Tooltip>}>
                      <i className={(isFavorite) ? "fas fa-heart mb-3 d-block cursor-pointer active" : "far d-block cursor-pointer mb-3 fa-heart"} onClick={() => setIsFavorite((isFavorite) ? false : true)}></i>
                    </OverlayTrigger>
                    <OverlayTrigger placement="left" overlay={<Tooltip  id="tooltip-share">Share</Tooltip>}>
                      <i className="fas fa-share-alt d-block mb-3 cursor-pointer"></i>
                    </OverlayTrigger>
                    <OverlayTrigger placement="left" overlay={<Tooltip  id="tooltip-map">Map</Tooltip>}>
                      <i className="fas fa-map-marker-alt mb-3 d-block cursor-pointer"></i>
                    </OverlayTrigger>
                    <OverlayTrigger placement="left" overlay={<Tooltip  id="tooltip-whatsapp">WhatsApp</Tooltip>}>
                      <i className="fab fa-whatsapp d-block mb-3 cursor-pointer"></i>
                    </OverlayTrigger>
                  </div>
                  </>
                }
           </div>
        </div>
    )
}

export default ProfessionalSingle;
