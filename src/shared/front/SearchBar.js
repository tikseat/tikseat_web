import React from 'react';
import { Link } from "react-router-dom";

const SearchBar = () => (
    <div className="search-input-container">
        <Link to="/listing"><i className="fas fa-search search-icon"></i></Link>
        <input type="text" name="search" placeholder="Search Service, Salon or Stylist" />
    </div>
);

export default SearchBar;
