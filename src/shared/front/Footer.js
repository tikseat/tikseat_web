import React from 'react';
import { NavLink } from 'react-router-dom';
import Logo from '../../assets/img/logo.png';
import { Link } from "react-router-dom";

const Footer = () => (
    <div className="container d-flex align-items-center justify-content-between h-100">
        <div className="menu-container flex-1 d-flex align-items-center justify-content-center">
            <NavLink to="/about" className="menu-item" activeClassName="active">About</NavLink>
            <NavLink to="/contact" className="menu-item" activeClassName="active" >Contact</NavLink>
            <NavLink to="/term-policy" className="menu-item" activeClassName="active">Terms Policies</NavLink>
        </div>
    </div>
);

export default Footer;
