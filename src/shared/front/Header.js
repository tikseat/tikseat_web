import React from 'react';
import { NavLink } from 'react-router-dom';
import Logo from '../../assets/img/logo.png';
import { Link } from "react-router-dom";
import SearchBar from './SearchBar';

// const toggleMenu = (event) => {
//     event.preventDefault();
//     let sidebarElement = document.getElementsByClassName("sidebar");
//     let classList = sidebarElement[0].classList;
//     classList.remove("d-md-none")
//     classList.add("d-md-block")
//     document.getElementById("wrapper").classList.toggle("toggled");
// }

const Header = (props) => {
    return (
        <div className="container d-flex align-items-center justify-content-between h-100">
            <div className="site-logo">
                <Link to="/"><img src={Logo} alt="Tikseat" width="150" /></Link>
            </div>
            {props.config.isSearchBarEnabled &&
            <div className="header-search-bar">
                <SearchBar />
            </div>
            }
            <div className="actions-container">
                <Link to="/auth/signup" className="mr-3 text-light">Free Listing</Link>
                <Link to="/auth/signup"><button className="btn btn-secondary"><i className="fas fa-briefcase text-light"></i> For Business</button></Link>
            </div>
        </div>
    );
}
export default Header;
