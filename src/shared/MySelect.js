import React from 'react';
import { withFormsy } from 'formsy-react';

const MySelect = (props) => {
  const changeValue = (event) => {
    props.setValue(event.currentTarget.value);
    if (props.selecteChange) {
      props.selecteChange(event.currentTarget.value);
    }
  }

  const className = 'form-group' + (props.className || ' ') +
    (props.showRequired() ? 'required' : props.showError() ? 'error' : '');
  const errorMessage = props.getErrorMessage();

  const options = props.options.map((option, i) => (
    <option key={option.title + option.value} value={option.value}>
      {option.title}
    </option>
  ));
  return (
    <div className={className}>
      <label htmlFor={props.name}>{props.title}</label>
      <select name={props.name} onChange={changeValue} value={props.getValue() || ''}>
        {options}
      </select>
      <div className="select-arrow">
        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="11.1" viewBox="0 0 18 11.1">
          <g id="keyboard-right-arrow-button" transform="translate(18) rotate(90)">
            <g id="chevron-right" transform="translate(0)">
              <path id="Path_266" data-name="Path 266" d="M2.1,0,0,2.1,6.9,9,0,15.9,2.1,18l9-9Z" />
            </g>
          </g>
        </svg>

      </div>
      <span className='validation-error'>{errorMessage}</span>
    </div>
  );
}


export default withFormsy(MySelect);