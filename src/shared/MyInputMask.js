import { withFormsy } from 'formsy-react';
import React from 'react';
import InputMask from 'react-input-mask';

const MyInputMask = (props) => {

  const changeValue = (event) => {
    props.setValue(event.currentTarget.value);
  }

  const errorMessage = props.getErrorMessage();
  const { id, name, label, className, placeholder, mask, groupClassName } = props;
  return (
    <div className={groupClassName}>
      <label>{label} {props.isRequired() ? '*' : null}</label>
      <InputMask
        onChange={changeValue}
        id={id}
        mask={mask}
        name={name}
        className={className}
        value={props.getValue() || ''}
        placeholder={placeholder}
        maskChar={null}
      />
      <small style={{ color: 'red' }}>{errorMessage}</small>
    </div>
  );
}

export default withFormsy(MyInputMask);