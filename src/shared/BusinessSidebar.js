import React from 'react';
import { NavLink } from 'react-router-dom';
import routes from './../core/routes';
import logo from '../assets/img/logo.png';

const BusinessSidebar = () => (
    <div className="sidebar d-xs-block d-sm-block d-md-none d-lg-block">
        <div className="logo-icon d-flex align-items-center justify-content-between">
            <img src={logo} alt="Tikseat" />
            <span>Business</span>
        </div>
        <div className="list-group list-group-flush">
            {routes
                .filter((prop) => prop.layout === "business" && prop.isSidebarMenu)
                .map((prop, key) => {
                    return (
                        <NavLink
                            to={{
                                pathname: prop.path,
                                state: { title: prop.name }
                            }}
                            key={key}
                            className="list-group-item list-group-item-action"
                            activeClassName="active"
                        >
                            <i className={prop.icon}></i>{prop.name}
                        </NavLink>
                    );
                })}
        </div>
    </div>
);

export default BusinessSidebar;
