import './../assets/css/style.css';
import './../assets/scss/admin.scss';

import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import routes from './../core/routes';
import Navbar from './../shared/Navbar';
import Sidebar from './../shared/Sidebar';

const switchRoutes = (
    <Switch>
        {routes
            .filter((prop) => prop.layout === "admin")
            .map((prop, key) => {
                return (
                    <Route
                        path={prop.path}
                        component={prop.component}
                        key={key}
                    />
                );
            })}
        <Route
            // exact
            path="/"
            render={() => {
                return <Redirect to={`/admin/dashboard`} />;
            }}
        />
    </Switch>
);


const Admin = () => (
    <div className="d-flex" id="wrapper">
        <Sidebar />
        <div className="content">
            {/* <Navbar /> */}
            {switchRoutes}
        </div>
    </div>
);

export default Admin;
