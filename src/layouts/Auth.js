import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import routes from './../core/routes';

const switchRoutes = (
    <Switch>
        {routes
            .filter((prop) => prop.layout === "auth")
            .map((prop, key) => {
                return (
                    <Route
                        path={prop.path}
                        component={prop.component}
                        key={key}
                    />
                );
            })}
        <Route
            // exact
            path="/auth"
            render={() => {
                return <Redirect to={`/auth/login`} />;
            }}
        />
    </Switch>
);


const Admin = () => (
    <div>
        {switchRoutes}
    </div>
);

export default Admin;
