import './../assets/css/style.css';
import './../assets/css/style.min.css';

import React from 'react';
import { Route, Switch, Link } from 'react-router-dom';
import routes from './../core/routes';

import Header from '../shared/front/Header';
import Home from '../pages/front/Home';
import '../assets/scss/front.scss';
// import FrontSidebar from '../shared/front/FrontSidebar';
// import CategoryBar from '../shared/front/CategoryBar';
// import SearchBar from '../shared/front/SearchBar';
import Banner from '../shared/front/Banner';
import Footer from '../shared/front/Footer';

const switchRoutes = (
    <Switch>
        {routes 
            .filter((prop) => prop.layout === "front")
            .map((prop, key) => {
                return (
                    <Route
                        path={prop.path}
                        component={prop.component}
                        key={key}
                    />
                );
            })}
    </Switch>
);


const Front = (props) => (
    
    <div className="wrapper">
        {console.log(props, 'props')}
        <div className="header header-shadow">
            <Header config={{isSearchBarEnabled : (props.location.pathname !== '/' && props.location.pathname !== '/home') ? true : false}} />
        </div>
        <div className="main-container">
            {(props.location.pathname === '/' || props.location.pathname === '/home') &&
                <Banner config={{isSilderEnalbed: true, isSearchBarEnabled: true}} />
            }
            <div className="container page-wrapper">
                {switchRoutes}
                    <Route
                        exact
                        path={props.match.path}
                        render={() => {
                            return (
                                <Home />
                            )
                        }}
                />
            </div>
            <div className="footer">
                <Footer />
            </div>
        </div>
    </div>
);

export default Front;
