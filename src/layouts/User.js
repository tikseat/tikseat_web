import './../assets/css/style.min.css';
import './../assets/css/style.css';

import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import routes from './../core/routes';
import Navbar from './../shared/Navbar';
import Sidebar from './../shared/Sidebar';
import UserSidebar from './../shared/UserSidebar';

const switchRoutes = (
    <Switch>
        {routes
            .filter((prop) => prop.layout === "user")
            .map((prop, key) => {
                return (
                    <Route
                        path={prop.path}
                        component={prop.component}
                        key={key}
                    />
                );
            })}
        <Route
            // exact
            path="/"
            render={() => {
                return <Redirect to={`/user/profile`} />;
            }}
        />
    </Switch>
);


const User = () => (
    <div className="d-flex" id="wrapper">
        {/* <UserSidebar /> */}
        <div className="content">
            {/* <Navbar /> */}
            {switchRoutes}
        </div>
    </div>
);

export default User;
