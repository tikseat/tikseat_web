import './../assets/css/style.css';
import './../assets/scss/admin.scss';

import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import routes from './../core/routes';
import Navbar from './../shared/Navbar';
import BusinessSidebar from '../shared/BusinessSidebar';

const switchRoutes = (
    <Switch>
        {routes
            .filter((prop) => prop.layout === "business")
            .map((prop, key) => {
                return (
                    <Route
                        path={prop.path}
                        component={prop.component}
                        key={key}
                    />
                );
            })}
        <Route
            // exact
            path="/"
            render={() => {
                return <Redirect to={`/business/dashboard`} />;
            }}
        />
    </Switch>
);


const Business = () => (
    <div className="d-flex" id="wrapper">
        <BusinessSidebar />
        <div className="content">
            {/* <Navbar /> */}
            {switchRoutes}
        </div>
    </div>
);

export default Business;
