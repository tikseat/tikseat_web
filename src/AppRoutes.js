import React, { useEffect } from 'react';
import { withRouter } from 'react-router';
import { Route, Switch, Redirect } from 'react-router-dom';

import Core from './core/core.main';
import Admin from './layouts/Admin';
import Auth from './layouts/Auth';
import { isAuthenticated, getAuthenticatedUser } from './core/auth';

import store from './store/createStore';
import * as actionCreators from './store/actions';
import Front from './layouts/Front';
import Login from './pages/Login';
import User from './layouts/User';
import Business from './layouts/Business';

const AppRoutes = (props) => {

  const addPageTitle = (location) => {
    if (location && location.state) {
      if (location.state.title) {
        document.title = `${location.state.title} | ${process.env.REACT_APP_NAME}`;
      } else {
        document.title = `${process.env.REACT_APP_NAME}`;
      }
    } else {
      document.title = `${process.env.REACT_APP_NAME}`;
    }
  }

  useEffect(() => {
    if (props.history) {
      addPageTitle(props.location);
      props.history.listen((location, action) => {
        // remove server errors when page is changed
        const storeData = store.getState();
        if (storeData && storeData.auth.serverErrors) {
          store.dispatch(actionCreators.clearErrors());
        }
        // remove server errors when page is changed
        if (document.getElementsByClassName('toggled').length > 0) {
          document.getElementById("wrapper").classList.toggle("toggled");
        }
        setTimeout(() => {
          window.$(".selectpicker").selectpicker();
        }, 100);
        addPageTitle(location);
      });
    }
  }, []);


  return (
    <>
      <Core />
      <Switch>
        <OpenRoute path="/auth" component={Auth} />
        <BussinessPrivateRoute path="/business" component={Business} />
        <PrivateRoute path="/admin" component={Admin} />
        <UserPrivateRoute path="/user" component={User} />

        <Route path="/" component={Front} />
        <Route render={() => <h1>Not found</h1>} />
      </Switch>
    </>
  );
}

const PrivateRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => isAuthenticated() === true && getAuthenticatedUser().userType === 'admin'
        ? <Component {...props} />
        : <Redirect to={{ pathname: '/auth/login', state: { from: props.location }, fromURL: props.location }} />}
    />
  )
}

const BussinessPrivateRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => isAuthenticated() === true && getAuthenticatedUser().userType === 'bussiness'
        ? <Component {...props} />
        : <Redirect to={{ pathname: '/auth/login', state: { from: props.location }, fromURL: props.location }} />}
    />
  )
}

const UserPrivateRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => isAuthenticated() === true && getAuthenticatedUser().userType === 'user'
        ? <Component {...props} />
        : <Redirect to={{ pathname: '/auth/login', state: { from: props.location }, fromURL: props.location }} />}
    />
  )
}

const OpenRoute = ({ component: Component, ...rest }) => {
  if (isAuthenticated() === false) {
    return (
      <Route
        {...rest}
        render={(props) => <Component {...props} />}
      />
    )
  } else if(isAuthenticated() === true && getAuthenticatedUser().userType === 'admin') {
    return (
      <Route
        {...rest}
        render={(props) => <Redirect to={{ pathname: '/admin/dashboard', state: { from: props.location }, fromURL: props.location }} />}
      />
    )
  }else if(isAuthenticated() === true && getAuthenticatedUser().userType === 'bussiness') {
    return (
        <Route
          {...rest}
          render={(props) => <Redirect to={{ pathname: '/business/dashboard', state: { from: props.location }, fromURL: props.location }} />}
        />
      )
  }else if(isAuthenticated() === true && getAuthenticatedUser().userType === 'user') {
    return (
      <Route
        {...rest}
        render={(props) => <Redirect to={{ pathname: '/', state: { from: props.location }, fromURL: props.location }} />}
      />
    )
  }
}


export default withRouter(AppRoutes);



